<?php
include __DIR__ . '/../../core/System.php';
System::init(['ENV' => 'www']);

System::check_value_empty($_POST, ['your-email', 'your-name']);
$newsletter = System::readJsonFile(__DIR__ . '/newletter.json');

$newsletter[$_POST['your-email']] = ['email' => $_POST['your-email'], 'name' => $_POST['your-name']];

System::writeJsonFile(__DIR__ . '/newletter.json', $newsletter);

JsonResponse::sendResponse('Completed.');
