<div id="qodef-page-outer" style="padding-top: 0px;">
    <div class="qodef-page-title qodef-m qodef-title--standard qodef-alignment--center qodef-vertical-alignment--window-top qodef--has-image qodef-image--parallax qodef-parallax">
        <div class="qodef-m-inner">
            <div class="qodef-parallax-img-holder" style="opacity: 1;">
                <div class="qodef-parallax-img-wrapper" style="transform: translate3d(0px, 35.92%, 0px);"><img
                            width="1920" height="1200"
                            src="themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1.jpg"
                            class="qodef-parallax-img" alt="aa"
                            srcset="themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1.jpg 1920w, themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1-300x188.jpg 300w, themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1-1024x640.jpg 1024w, themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1-768x480.jpg 768w, themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1-1536x960.jpg 1536w, themes/singlemalt/wp-content/uploads/2020/10/p3-background-img-1-600x375.jpg 600w"
                            sizes="(max-width: 1920px) 100vw, 1920px"></div>
            </div>
            <div class="qodef-m-content qodef-content-grid qodef-parallax-content-holder">
                <h1 class="qodef-m-title entry-title"><?= System::translate('Subscribe to our newsletter') ?></h1>
            </div>
        </div>
    </div>
    <div id="qodef-page-inner" class="qodef-content-full-width">
        <main id="qodef-page-content" class="qodef-grid qodef-layout--template ">
            <div class="qodef-grid-inner clear">
                <div class="qodef-grid-item qodef-page-content-section qodef-col--12">
                    <div data-elementor-type="wp-page" data-elementor-id="2218" class="elementor elementor-2218"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-1144524 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="1144524" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c999468"
                                                 data-id="c999468" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7ba2358 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
                                                                data-id="7ba2358" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                                <div class="elementor-row">
                                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-c151f7c"
                                                                         data-id="c151f7c" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-02bba57 elementor-widget elementor-widget-singlemalt_core_section_title"
                                                                                     data-id="02bba57"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="singlemalt_core_section_title.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation qodef--appeared">
<span class="qodef-m-caption" style="margin: 0px 0px 11px 0px">
<span class="qodef-caption-line qodef--left"></span>
<?= System::translate('Subscribe to our newsletter') ?> <span class="qodef-caption-line qodef--right"></span>
</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-a8cd88d elementor-widget elementor-widget-wp-widget-singlemalt_core_contact_form_7">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="qodef-contact-form-7">
                                                                                            <div role="form"
                                                                                                 class="wpcf7"
                                                                                                 id="wpcf7-f5-p2218-o1"
                                                                                                 lang="en-US" dir="ltr">
                                                                                                <div class="screen-reader-response"
                                                                                                     role="alert"
                                                                                                     aria-live="polite"></div>
                                                                                                <form uri="ajax/newsletter/subscribe.php"
                                                                                                      method="post"
                                                                                                      class="init">
                                                                                                    <div class="qodef-contact-form-simple">
                                                                                                        <div class="qodef-contact-form-simple-input">
                                                                                                            <label>
                                                                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                                                                    <input type="email"
                                                                                                                           name="your-email"
                                                                                                                           value="<?= $_GET['your-mail'] ?? '' ?>"
                                                                                                                           size="40"
                                                                                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                                           aria-required="true"
                                                                                                                           aria-invalid="false"
                                                                                                                           placeholder="E-mail" required>
                                                                                                                </span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="qodef-contact-form-simple-input">
                                                                                                            <label><span
                                                                                                                        class="wpcf7-form-control-wrap your-name"><input
                                                                                                                            type="text"
                                                                                                                            name="your-name"
                                                                                                                            value=""
                                                                                                                            size="40"
                                                                                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                                            aria-required="true"
                                                                                                                            aria-invalid="false"
                                                                                                                            placeholder="Name" required></span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="qodef-contact-form-simple-submit">
                                                                                                            <button type="submit"
                                                                                                                    class="wpcf7-form-control wpcf7-submit qodef-button qodef-size--normal qodef-type--filled qodef-m">
                                                                                                                <span class="qodef-m-text"><?= System::translate('Submit') ?></span>
                                                                                                            </button>
                                                                                                            <span class="ajax-loader"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="wpcf7-response-output"
                                                                                                         role="alert"
                                                                                                         aria-hidden="true"></div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
