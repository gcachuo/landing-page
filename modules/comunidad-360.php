<div>
    <div class="qodef-page-title qodef-m qodef-title--standard qodef-alignment--center qodef-vertical-alignment--window-top qodef--has-image qodef-image--parallax qodef-parallax">
        <div class="qodef-m-inner">
            <div class="qodef-parallax-img-holder">
                <div class="qodef-parallax-img-wrapper">
                    <img width="1920" height="1112"
                         src="assets/images/IMG%2018.jpg"
                         class="qodef-parallax-img" alt="aa" sizes="(max-width: 1920px) 100vw, 1920px"/>
                </div>
            </div>
        </div>
    </div>
    <div id="qodef-page-inner" class="qodef-content-full-width">
        <main id="qodef-page-content" class="qodef-grid qodef-layout--template ">
            <div class="qodef-grid-inner clear">
                <div class="qodef-grid-item qodef-page-content-section qodef-col--12">
                    <div data-elementor-type="wp-page" data-elementor-id="3119" class="elementor elementor-3119"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-0a3b219 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="0a3b219" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-66c14b1"
                                                 data-id="66c14b1" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-029769f elementor-widget elementor-widget-singlemalt_core_section_title"
                                                             data-id="029769f" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_section_title.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                    <span class="qodef-m-caption"
                                                                          style="margin: 0 0 14px 0">
                                                                    <span class="qodef-caption-line qodef--left"></span>
                                                                    <?= System::translate('Comunidad 360') ?>
                                                                        <span class="qodef-caption-line qodef--right"></span>
                                                                    </span>
                                                                    <h1 class="qodef-m-title">
                                                                        <?= System::translate('Responsabilidad social') ?>
                                                                    </h1>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('responsabilidad-social:description') ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-0a3b219 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="0a3b219" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-66c14b1"
                                                 data-id="66c14b1" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-029769f elementor-widget elementor-widget-singlemalt_core_section_title"
                                                             data-id="029769f" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_section_title.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                    <span class="qodef-m-caption"
                                                                          style="margin: 0 0 14px 0">
                                                                    <span class="qodef-caption-line qodef--left"></span>
                                                                    <?= System::translate('Comunidad 360') ?>
                                                                        <span class="qodef-caption-line qodef--right"></span>
                                                                    </span>
                                                                    <h1 class="qodef-m-title">
                                                                        <?= System::translate('Medio Ambiente') ?>
                                                                    </h1>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('medio-ambiente:description:0') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('medio-ambiente:description:1') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('medio-ambiente:description:2') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('medio-ambiente:description:3') ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-0a3b219 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="0a3b219" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-66c14b1"
                                                 data-id="66c14b1" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-029769f elementor-widget elementor-widget-singlemalt_core_section_title"
                                                             data-id="029769f" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_section_title.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                    <span class="qodef-m-caption"
                                                                          style="margin: 0 0 14px 0">
                                                                    <span class="qodef-caption-line qodef--left"></span>
                                                                    <?= System::translate('Comunidad 360') ?>
                                                                        <span class="qodef-caption-line qodef--right"></span>
                                                                    </span>
                                                                    <h1 class="qodef-m-title">
                                                                        <?= System::translate('Grupos Vulnerables') ?>
                                                                    </h1>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('grupos-vulnerables:description:0') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('grupos-vulnerables:description:1') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('grupos-vulnerables:description:2') ?>
                                                                    </p>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px 0 0 0">
                                                                        <?= System::translate('grupos-vulnerables:description:3') ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
