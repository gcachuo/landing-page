<div id="qodef-page-outer">
    <div class="qodef-page-title qodef-m qodef-title--standard qodef-alignment--center qodef-vertical-alignment--window-top qodef--has-image qodef-image--parallax qodef-parallax">
        <div class="qodef-m-inner">
            <div class="qodef-parallax-img-holder">
                <div class="qodef-parallax-img-wrapper">
                    <img width="1920" height="1200"
                         src="assets/images/IMG%2019.jpg"
                         class="qodef-parallax-img" alt="aa" sizes="(max-width: 1920px) 100vw, 1920px"/>
                </div>
            </div>
            <div class="qodef-m-content qodef-content-grid qodef-parallax-content-holder">
                <h1 class="qodef-m-title entry-title">
                    <?= System::translate('Contactanos') ?>
                </h1>
            </div>
        </div>
    </div>
    <div id="qodef-page-inner" class="qodef-content-full-width">
        <main id="qodef-page-content" class="qodef-grid qodef-layout--template ">
            <div class="qodef-grid-inner clear">
                <div class="qodef-grid-item qodef-page-content-section qodef-col--12">
                    <div data-elementor-type="wp-page" data-elementor-id="2218" class="elementor elementor-2218"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-1144524 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="1144524" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c999468"
                                                 data-id="c999468" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-4e7cb32 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
                                                                data-id="4e7cb32" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                                <div class="elementor-row">
                                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-110efdb"
                                                                         data-id="110efdb" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-095aced elementor-widget elementor-widget-text-editor"
                                                                                     data-id="095aced"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <h4 style="text-align: center;">
                                                                                                ayrshire</h4></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-d51a288 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="d51a288"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p style="text-align: center;">
                                                                                                <span style="line-height: 29px;"><a
                                                                                                            href="https://www.google.com/maps/place/Flanigan+Oaks+Dr,+North+Urban,+WV+26386,+USA/@39.3395203,-80.4025281,17z/data=!4m5!3m4!1s0x884a6c7837a6f2c5:0x6ed297ff13d9fd06!8m2!3d39.3395162!4d-80.4003394"
                                                                                                            target="_blank"
                                                                                                            rel="noopener">549 Flanigan Oaks Drive</a><br/><a
                                                                                                            href="tel:2025550132">202-555-0132</a>, <a
                                                                                                            href="tel:2025550133">202-555-0133</a><br/><a
                                                                                                            href="/cdn-cgi/l/email-protection#6112080f060d040c000d1521100e0504080f1504130002150817044f020e0c"><span
                                                                                                                class="__cf_email__"
                                                                                                                data-cfemail="b6c5dfd8d1dad3dbd7dac2f6c7d9d2d398d5d9db">[email&#160;protected]</span></a></span>
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-849a420"
                                                                         data-id="849a420" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-a18e447 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="a18e447"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <h4 style="text-align: center;">
                                                                                                ullapool</h4></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-277350d elementor-widget elementor-widget-text-editor"
                                                                                     data-id="277350d"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p style="text-align: center;">
                                                                                                <span style="line-height: 29px;"><a
                                                                                                            href="https://www.google.com/maps/place/1907+Oakmount+Rd,+South+Euclid,+OH+44121,+USA/@41.5082576,-81.5158027,17z/data=!3m1!4b1!4m5!3m4!1s0x8831020af6423b5f:0xda3754ffafa77c70!8m2!3d41.5082576!4d-81.513614"
                                                                                                            target="_blank"
                                                                                                            rel="noopener">1907 Oakmound Road</a><br/><a
                                                                                                            href="tel:7733184420">773-319-4420</a>, <a
                                                                                                            href="tel:3315516725">331-551-6725</a><br/><a
                                                                                                            href="/cdn-cgi/l/email-protection#e7948e89808b828a868b93a7968883828e899382958684938e9182c984888a"><span
                                                                                                                class="__cf_email__"
                                                                                                                data-cfemail="7a0913141d161f171b160e3a0b151e1f54191517">[email&#160;protected]</span></a></span>
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8082fbb"
                                                                         data-id="8082fbb" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-4ff0a9a elementor-widget elementor-widget-text-editor"
                                                                                     data-id="4ff0a9a"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <h4 style="text-align: center;">
                                                                                                tayside</h4></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-06c94ff elementor-widget elementor-widget-text-editor"
                                                                                     data-id="06c94ff"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p style="text-align: center;">
                                                                                                <span style="line-height: 29px;"><a
                                                                                                            href="https://www.google.com/maps/place/Spring+St,+New+York,+NY,+USA/@40.7243844,-74.0037306,17z/data=!3m1!4b1!4m5!3m4!1s0x89c2598c74796b0b:0x5e7aba8d9d7e19fd!8m2!3d40.7243804!4d-74.0015419"
                                                                                                            target="_blank"
                                                                                                            rel="noopener">4581 Spring Street</a><br/><a
                                                                                                            href="tel:2177961454">217-796-1454</a>, <a
                                                                                                            href="tel:7089947054">708-994-7054</a><br/><a
                                                                                                            href="/cdn-cgi/l/email-protection#55263c3b3239303834392115243a31303c3b2130273436213c23307b363a38"><span
                                                                                                                class="__cf_email__"
                                                                                                                data-cfemail="3043595e575c555d515c4470415f54551e535f5d">[email&#160;protected]</span></a></span>
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-2cd1a2e"
                                                                         data-id="2cd1a2e" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-1b0344e elementor-widget elementor-widget-text-editor"
                                                                                     data-id="1b0344e"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <h4 style="text-align: center;">
                                                                                                lothian</h4></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-eec07b3 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="eec07b3"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p style="text-align: center;">
                                                                                                <span style="line-height: 29px;"><a
                                                                                                            href="https://www.google.com/maps/place/4851+E+Eden+Dr,+Cave+Creek,+AZ+85331,+USA/@33.7559411,-111.9791467,17z/data=!3m1!4b1!4m5!3m4!1s0x872b7a4a12cc0529:0xe8c6288a9051db66!8m2!3d33.7559411!4d-111.976958"
                                                                                                            target="_blank"
                                                                                                            rel="noopener">4851 Eden Drive</a><br/><a
                                                                                                            href="tel:8048948838">804-894-8838</a>, <a
                                                                                                            href="te:7579273164">757-927-3164</a><br/><a
                                                                                                            href="/cdn-cgi/l/email-protection#bdced4d3dad1d8d0dcd1c9fdccd2d9d8d4d3c9d8cfdcdec9d4cbd893ded2d0"><span
                                                                                                                class="__cf_email__"
                                                                                                                data-cfemail="2f5c464148434a424e435b6f5e404b4a014c4042">[email&#160;protected]</span></a></span>
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7ba2358 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
                                                                data-id="7ba2358" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                                <div class="elementor-row">
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-c151f7c"
                                                                         data-id="c151f7c" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-02bba57 elementor-widget elementor-widget-singlemalt_core_section_title"
                                                                                     data-id="02bba57"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="singlemalt_core_section_title.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
<span class="qodef-m-caption" style="margin: 0px 0px 11px 0px">
<span class="qodef-caption-line qodef--left"></span>
<?= System::translate('Contactanos') ?>
    <span class="qodef-caption-line qodef--right"></span>
</span>
                                                                                            <h2 class="qodef-m-title"
                                                                                                style="color: #232323">
                                                                                                get in touch </h2>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-a8cd88d elementor-widget elementor-widget-wp-widget-singlemalt_core_contact_form_7"
                                                                                     data-id="a8cd88d"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="wp-widget-singlemalt_core_contact_form_7.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="qodef-contact-form-7">
                                                                                            <div role="form"
                                                                                                 class="wpcf7"
                                                                                                 id="wpcf7-f5-p2218-o1"
                                                                                                 lang="en-US" dir="ltr">
                                                                                                <div class="screen-reader-response"
                                                                                                     role="alert"
                                                                                                     aria-live="polite"></div>
                                                                                                <form action="/contact-us/#wpcf7-f5-p2218-o1"
                                                                                                      method="post"
                                                                                                      class="wpcf7-form init"
                                                                                                      novalidate="novalidate">
                                                                                                    <div style="display: none;">
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7"
                                                                                                               value="5"/>
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7_version"
                                                                                                               value="5.2.2"/>
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7_locale"
                                                                                                               value="en_US"/>
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7_unit_tag"
                                                                                                               value="wpcf7-f5-p2218-o1"/>
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7_container_post"
                                                                                                               value="2218"/>
                                                                                                        <input type="hidden"
                                                                                                               name="_wpcf7_posted_data_hash"
                                                                                                               value=""/>
                                                                                                    </div>
                                                                                                    <div class="qodef-contact-form-simple">
                                                                                                        <div class="qodef-contact-form-simple-input">
                                                                                                            <label><span
                                                                                                                        class="wpcf7-form-control-wrap your-email"><input
                                                                                                                            type="email"
                                                                                                                            name="your-email"
                                                                                                                            value=""
                                                                                                                            size="40"
                                                                                                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                                            aria-required="true"
                                                                                                                            aria-invalid="false"
                                                                                                                            placeholder="E-mail"/></span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="qodef-contact-form-simple-input">
                                                                                                            <label><span
                                                                                                                        class="wpcf7-form-control-wrap your-name"><input
                                                                                                                            type="text"
                                                                                                                            name="your-name"
                                                                                                                            value=""
                                                                                                                            size="40"
                                                                                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                                            aria-required="true"
                                                                                                                            aria-invalid="false"
                                                                                                                            placeholder="Name"/></span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="qodef-contact-form-simple-text">
                                                                                                            <label>
                                                                                                                <span class="wpcf7-form-control-wrap your-message"><textarea
                                                                                                                            name="your-message"
                                                                                                                            cols="40"
                                                                                                                            rows="6"
                                                                                                                            class="wpcf7-form-control wpcf7-textarea"
                                                                                                                            aria-invalid="false"
                                                                                                                            placeholder="Message"></textarea></span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="qodef-contact-form-simple-submit">
                                                                                                            <button type="submit"
                                                                                                                    class="wpcf7-form-control wpcf7-submit qodef-button qodef-size--normal qodef-type--filled qodef-m">
                                                                                                                <span class="qodef-m-text">Submit</span>
                                                                                                            </button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="wpcf7-response-output"
                                                                                                         role="alert"
                                                                                                         aria-hidden="true"></div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-773865b"
                                                                         data-id="773865b" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-2ef3595 elementor-widget elementor-widget-singlemalt_core_google_map"
                                                                                     data-id="2ef3595"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="singlemalt_core_google_map.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="qodef-shortcode qodef-m  qodef-google-map">
                                                                                            <div class="qodef-m-map"
                                                                                                 id="qodef-map-id--1"
                                                                                                 data-addresses='["4851 E Eden Dr, Cave Creek, AZ 85331, USA"]'
                                                                                                 data-pin=https://singlemalt.qodeinteractive.com/wp-content/uploads/2020/10/p3-pin-img-1.png
                                                                                                 data-unique-id=1
                                                                                                 data-height=450></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
