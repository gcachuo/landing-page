<section
        class="elementor-section elementor-top-section elementor-element elementor-element-931d60d elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
        data-id="931d60d" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e5f8f5f"
                 data-id="e5f8f5f" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-896bc67 elementor-widget elementor-widget-slider_revolution"
                             data-id="896bc67" data-element_type="widget"
                             data-widget_type="slider_revolution.default">
                            <div class="elementor-widget-container">
                                <div class="wp-block-themepunch-revslider">
                                    <p class="rs-p-wp-fix"></p>
                                    <rs-module-wrap id="rev_slider_1_1_wrapper"
                                                    data-source="gallery"
                                                    style="background:#232323; padding:0; background-image:url(assets/images/IMG%204.jpg); background-repeat:no-repeat;background-size:cover; background-position:center center;">
                                        <rs-module id="rev_slider_1_1" style=""
                                                   data-version="6.2.23">
                                            <rs-slides>
                                                <rs-slide data-key="rs-1" data-title="Slide"
                                                          data-thumb="assets/images/IMG%204.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/IMG%204.jpg"
                                                         alt="jj" title="h1-rev-img-1" width="1920" height="1080"
                                                         class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:11px,11px,0,6px;y:m;yo:-44px,-4px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,571px,500px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate('Margarita Clásica') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-91px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Recetas de Oro') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                </rs-slide>
                                                <rs-slide data-key="rs-42" data-title="Slide"
                                                          data-thumb="assets/images/IMG%2010.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/IMG%2010.jpg"
                                                         alt="jj" title="h1-rev-background-img-2" width="1920"
                                                         height="1080" class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:8px,10px,8px,5px;y:m;yo:-44px,-4px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,571px,502px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate('Mexican Mule') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-91px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Recetas de Oro') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                </rs-slide>
                                                <rs-slide data-key="rs-43" data-title="Slide"
                                                          data-thumb="assets/images/img%2011.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/img%2011.jpg"
                                                         alt="jj" title="h1-background-img-3" width="1920" height="1080"
                                                         class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:11px,10px,6px,5px;y:m;yo:-44px,-3px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,755px,600px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate('Paloma Rosa') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-90px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Recetas de Oro') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-88px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-88px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                </rs-slide>
                                            </rs-slides>
                                        </rs-module>
                                        <script type="text/javascript">
                                            setREVStartSize({
                                                c: 'rev_slider_1_1',
                                                rl: [1920, 1720, 1200, 600],
                                                el: [780, 640, 800, 500],
                                                gw: [1300, 1100, 600, 300],
                                                gh: [780, 640, 800, 500],
                                                type: 'standard',
                                                justify: '',
                                                layout: 'fullscreen',
                                                offsetContainer: '',
                                                offset: '',
                                                mh: "0"
                                            });
                                            var revapi1,
                                                tpj;

                                            function revinit_revslider11() {
                                                jQuery(function () {
                                                    tpj = jQuery;
                                                    revapi1 = tpj("#rev_slider_1_1");
                                                    if (revapi1 == undefined || revapi1.revolution == undefined) {
                                                        revslider_showDoubleJqueryError("rev_slider_1_1");
                                                    } else {
                                                        revapi1.revolution({
                                                            sliderLayout: "fullscreen",
                                                            duration: "3500ms",
                                                            visibilityLevels: "1920,1720,1200,600",
                                                            gridwidth: "1300,1100,600,300",
                                                            gridheight: "780,640,800,500",
                                                            perspective: 600,
                                                            perspectiveType: "local",
                                                            editorheight: "780,640,800,500",
                                                            responsiveLevels: "1920,1720,1200,600",
                                                            progressBar: {
                                                                disableProgressBar: true
                                                            },
                                                            navigation: {
                                                                onHoverStop: false,
                                                                arrows: {
                                                                    enable: true,
                                                                    tmp: "<svg x=\"0px\" y=\"0px\" width=\"33.85px\" height=\"19.958px\" viewBox=\"0 0 33.85 19.958\" enable-background=\"new 0 0 33.85 19.958\" xml:space=\"preserve\"><line fill=\"none\" stroke=\"#fff\" stroke-width=\"1.4\" stroke-miterlimit=\"10\" x1=\"0\" y1=\"9.979\" x2=\"33\" y2=\"9.979\"/><polyline fill=\"none\" stroke=\"#fff\" stroke-width=\"1.4\" stroke-miterlimit=\"10\" points=\"23.376,0.495 32.859,9.979 	23.376,19.463 \"/></svg>",
                                                                    style: "qodef-navigation-light",
                                                                    hide_onmobile: true,
                                                                    hide_under: "768px",
                                                                    left: {
                                                                        h_offset: 46
                                                                    },
                                                                    right: {
                                                                        h_offset: 46
                                                                    }
                                                                },
                                                                bullets: {
                                                                    enable: true,
                                                                    tmp: "",
                                                                    style: "qodef-bullets-light",
                                                                    v_offset: 50,
                                                                    space: 30
                                                                }
                                                            },
                                                            fallbacks: {
                                                                allowHTML5AutoPlayOnAndroid: true
                                                            },
                                                        });
                                                    }

                                                });
                                            } // End of RevInitScript
                                            var once_revslider11 = false;
                                            if (document.readyState === "loading") {
                                                document.addEventListener('readystatechange', function () {
                                                    if ((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider11) {
                                                        once_revslider11 = true;
                                                        revinit_revslider11();
                                                    }
                                                });
                                            } else {
                                                once_revslider11 = true;
                                                revinit_revslider11();
                                            }
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape(".tparrows.qodef-navigation-light%20%7B%0A%20%20%20%20background%3A%20transparent%3B%0A%20%20%09width%3A%2034px%3B%0A%20%09height%3A%2020px%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Abefore%20%7B%0A%20%20%20%20font-size%3A%200%3B%0A%20%20%20%20color%3A%20transparent%3B%0A%20%20%20%20line-height%3A%200%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%20svg%20line%20%7B%0A%20%20%09stroke-dasharray%3A%2033%3B%0A%20%20%09stroke-dashoffset%3A%200%3B%0A%20%20%09transition%3A%20stroke-dashoffset%20.6s%20cubic-bezier%280.55%2C%200.46%2C%200.05%2C%200.65%29%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%20svg%20polyline%20%7B%0A%20%20%09stroke-dasharray%3A%2027%3B%0A%20%20%09stroke-dashoffset%3A%200%3B%0A%20%20%09transition%3A%20stroke-dashoffset%20.6s%20cubic-bezier%280.55%2C%200.46%2C%200.05%2C%200.65%29%20.2s%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Ahover%20svg%20line%20%7B%0A%20%20%09stroke-dashoffset%3A%2066%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Ahover%20svg%20polyline%20%7B%0A%20%20%09stroke-dashoffset%3A%2054%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light.tp-leftarrow%20svg%20%7B%0A%09transform%3A%20rotate%28180deg%29%3B%0A%7D%0A.qodef-bullets-light%20.tp-bullet%20%7B%0A%09border-radius%3A%2050%25%3B%0A%20%20%09background-color%3A%20%23fff%3B%0A%20%20%09width%3A%205px%3B%0A%20%20%09height%3A%205px%3B%0A%20%20%09transition%3A%20transform%20.2s%20ease-out%3B%0A%7D%0A%0A.qodef-bullets-light%20.tp-bullet%3Ahover%20%7B%0A%20%09transform%3A%20scale%281.8%29%3B%0A%7D%0A%0A.qodef-bullets-light%20.tp-bullet.selected%20%7B%0A%09transform%3A%20scale%281.8%29%3B%0A%7D%0A%0A.qodef-bullets-light.qodef--dark%20.tp-bullet%7B%0A%09background-color%3A%20%23232323%3B%0A%7D%0A");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                    </rs-module-wrap>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
