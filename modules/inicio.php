<section
        class="elementor-section elementor-top-section elementor-element elementor-element-931d60d elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
        data-id="931d60d" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e5f8f5f"
                 data-id="e5f8f5f" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-896bc67 elementor-widget elementor-widget-slider_revolution"
                             data-id="896bc67" data-element_type="widget"
                             data-widget_type="slider_revolution.default">
                            <div class="elementor-widget-container">
                                <div class="wp-block-themepunch-revslider">
                                    <p class="rs-p-wp-fix"></p>
                                    <rs-module-wrap id="rev_slider_1_1_wrapper"
                                                    data-source="gallery"
                                                    style="background:#232323; padding:0; background-image:url(assets/images/IMG%201.jpg); background-repeat:no-repeat;background-size:cover; background-position:center center;">
                                        <rs-module id="rev_slider_1_1" style=""
                                                   data-version="6.2.23">
                                            <rs-slides>
                                                <rs-slide data-key="rs-1" data-title="Slide"
                                                          data-thumb="assets/images/IMG%201.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/IMG%201.jpg"
                                                         alt="jj" title="h1-rev-img-1" width="1920" height="1080"
                                                         class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:11px,11px,0,6px;y:m;yo:-44px,-4px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,571px,500px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate('Authentic Taste') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-91px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Transcendent flavor') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-1-layer-5"
                                                            data-type="text"
                                                            data-xy="x:c;xo:80px,93px,89px,93px;y:m;yo:83px,123px,175px,146px;"
                                                            data-text="w:normal;l:25,25,25,25;"
                                                            data-dim="w:361px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:300;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:12;font-family:Roboto;">
                                                        <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                           href="oro-liquido"
                                                           target="_self"
                                                           data-hover-color="#FFFFFF"
                                                           data-hover-background-color="#BC9D72"
                                                           data-hover-border-color="#BC9D72"
                                                           style="color: #FFFFFF;border-color: #BC9D72">
                                                        <span class="qodef-m-text">
                                                        <?= System::translate('explore') ?>
                                                        <span class="qodef-button-arrow-svg">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="14" height="7" viewBox="0 0 14.26 7.012"
                                                             enable-background="new 0 0 14.26 7.012"
                                                             xml:space="preserve">
                                                        <line fill="none" stroke-miterlimit="10" x1="0" y1="3.506"
                                                              x2="13.553" y2="3.506"/>
                                                        <polyline fill="none" stroke-miterlimit="10"
                                                                  points="10.4,0.354 13.553,3.507 10.4,6.659 "/>
                                                        </svg>
                                                        </span>
                                                        </span>
                                                        </a>
                                                    </rs-layer>
                                                </rs-slide>
                                                <rs-slide data-key="rs-42" data-title="Slide"
                                                          data-thumb="assets/images/IMG%202.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/IMG%202.jpg"
                                                         alt="jj" title="h1-rev-background-img-2" width="1920"
                                                         height="1080" class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:8px,10px,8px,5px;y:m;yo:-44px,-4px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,571px,502px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate('Special releases') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-91px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Transcendent flavor') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-89px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-42-layer-5"
                                                            data-type="text"
                                                            data-xy="x:c;xo:80px,93px,89px,93px;y:m;yo:83px,123px,175px,146px;"
                                                            data-text="w:normal;l:25,25,25,25;"
                                                            data-dim="w:361px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:300;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:12;font-family:Roboto;">
                                                        <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--light qodef-skin--default"
                                                           href="oro-liquido"
                                                           target="_self"
                                                           data-hover-border-color="#BC9D72"
                                                           style="border-color: #BC9D72">
                                                                        <span class="qodef-m-text">
                                                                        <?= System::translate('explore') ?>
                                                                        <span class="qodef-button-arrow-svg">
                                                                        <svg version="1.1"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             x="0px" y="0px"
                                                                             width="14" height="7"
                                                                             viewBox="0 0 14.26 7.012"
                                                                             enable-background="new 0 0 14.26 7.012"
                                                                             xml:space="preserve">
                                                                        <line fill="none" stroke-miterlimit="10" x1="0"
                                                                              y1="3.506" x2="13.553" y2="3.506"/>
                                                                        <polyline fill="none" stroke-miterlimit="10"
                                                                                  points="10.4,0.354 13.553,3.507 10.4,6.659 "/>
                                                                        </svg>
                                                                        </span>
                                                                        </span>
                                                        </a>
                                                    </rs-layer>
                                                </rs-slide>
                                                <rs-slide data-key="rs-43" data-title="Slide"
                                                          data-thumb="assets/images/img%203.jpg"
                                                          data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                    <img src="assets/images/img%203.jpg"
                                                         alt="jj" title="h1-background-img-3" width="1920" height="1080"
                                                         class="rev-slidebg" data-no-retina>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-0"
                                                            data-type="text"
                                                            data-xy="x:c;xo:11px,10px,6px,5px;y:m;yo:-44px,-3px,4px,3px;"
                                                            data-text="w:normal,normal,normal,normal;s:76,76,76,45;l:90,90,90,52;ls:19px,19px,19px,10px;a:center;"
                                                            data-dim="w:auto,auto,755px,600px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:200;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:8;font-family:Rakkas;text-transform:uppercase;">
                                                        <?= System::translate("Tequila's pleasure") ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-1"
                                                            data-type="text"
                                                            data-xy="x:c;xo:0,0,2px,-1px;y:m;yo:-131px,-90px,-127px,-98px;"
                                                            data-text="w:normal;s:23;l:22;a:center;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:100;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:9;font-family:Crimson Pro;font-style:italic;">
                                                        <?= System::translate('Transcendent flavor') ?>
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-2"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:-100px,-100px,-98px,-101px;y:m;yo:-129px,-88px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:-50;"
                                                            data-frame_1="e:power3.out;st:120;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:11;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-3"
                                                            data-type="shape"
                                                            data-xy="x:c;xo:98px,98px,100px,97px;y:m;yo:-129px,-88px,-125px,-96px;"
                                                            data-text="w:normal;"
                                                            data-dim="w:35px;h:1px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="x:50;"
                                                            data-frame_1="e:power3.out;st:100;sp:1000;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:10;background-color:#bc9d72;">
                                                    </rs-layer>
                                                    <rs-layer
                                                            id="slider-1-slide-43-layer-5"
                                                            data-type="text"
                                                            data-xy="x:c;xo:80px,93px,89px,93px;y:m;yo:83px,124px,175px,146px;"
                                                            data-text="w:normal;l:25,25,25,25;"
                                                            data-dim="w:361px;"
                                                            data-rsp_o="off"
                                                            data-rsp_bd="off"
                                                            data-frame_0="y:100%;"
                                                            data-frame_1="e:power3.out;st:300;sp:1200;"
                                                            data-frame_999="o:0;st:w;"
                                                            style="z-index:12;font-family:Roboto;">
                                                        <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                           href="oro-liquido"
                                                           target="_self"
                                                           data-hover-color="#FFFFFF"
                                                           data-hover-background-color="#BC9D72"
                                                           data-hover-border-color="#BC9D72"
                                                           style="color: #FFFFFF;border-color: #BC9D72">
                                                        <span class="qodef-m-text">
                                                        <?= System::translate('explore') ?>
                                                        <span class="qodef-button-arrow-svg">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="14" height="7" viewBox="0 0 14.26 7.012"
                                                             enable-background="new 0 0 14.26 7.012"
                                                             xml:space="preserve">
                                                        <line fill="none" stroke-miterlimit="10" x1="0" y1="3.506"
                                                              x2="13.553" y2="3.506"/>
                                                        <polyline fill="none" stroke-miterlimit="10"
                                                                  points="10.4,0.354 13.553,3.507 10.4,6.659 "/>
                                                        </svg>
                                                        </span>
                                                        </span>
                                                        </a>
                                                    </rs-layer>
                                                </rs-slide>
                                            </rs-slides>
                                        </rs-module>
                                        <script type="text/javascript">
                                            setREVStartSize({
                                                c: 'rev_slider_1_1',
                                                rl: [1920, 1720, 1200, 600],
                                                el: [780, 640, 800, 500],
                                                gw: [1300, 1100, 600, 300],
                                                gh: [780, 640, 800, 500],
                                                type: 'standard',
                                                justify: '',
                                                layout: 'fullscreen',
                                                offsetContainer: '',
                                                offset: '',
                                                mh: "0"
                                            });
                                            var revapi1,
                                                tpj;

                                            function revinit_revslider11() {
                                                jQuery(function () {
                                                    tpj = jQuery;
                                                    revapi1 = tpj("#rev_slider_1_1");
                                                    if (revapi1 == undefined || revapi1.revolution == undefined) {
                                                        revslider_showDoubleJqueryError("rev_slider_1_1");
                                                    } else {
                                                        revapi1.revolution({
                                                            sliderLayout: "fullscreen",
                                                            duration: "3500ms",
                                                            visibilityLevels: "1920,1720,1200,600",
                                                            gridwidth: "1300,1100,600,300",
                                                            gridheight: "780,640,800,500",
                                                            perspective: 600,
                                                            perspectiveType: "local",
                                                            editorheight: "780,640,800,500",
                                                            responsiveLevels: "1920,1720,1200,600",
                                                            progressBar: {
                                                                disableProgressBar: true
                                                            },
                                                            navigation: {
                                                                onHoverStop: false,
                                                                arrows: {
                                                                    enable: true,
                                                                    tmp: "<svg x=\"0px\" y=\"0px\" width=\"33.85px\" height=\"19.958px\" viewBox=\"0 0 33.85 19.958\" enable-background=\"new 0 0 33.85 19.958\" xml:space=\"preserve\"><line fill=\"none\" stroke=\"#fff\" stroke-width=\"1.4\" stroke-miterlimit=\"10\" x1=\"0\" y1=\"9.979\" x2=\"33\" y2=\"9.979\"/><polyline fill=\"none\" stroke=\"#fff\" stroke-width=\"1.4\" stroke-miterlimit=\"10\" points=\"23.376,0.495 32.859,9.979 	23.376,19.463 \"/></svg>",
                                                                    style: "qodef-navigation-light",
                                                                    hide_onmobile: true,
                                                                    hide_under: "768px",
                                                                    left: {
                                                                        h_offset: 46
                                                                    },
                                                                    right: {
                                                                        h_offset: 46
                                                                    }
                                                                },
                                                                bullets: {
                                                                    enable: true,
                                                                    tmp: "",
                                                                    style: "qodef-bullets-light",
                                                                    v_offset: 50,
                                                                    space: 30
                                                                }
                                                            },
                                                            fallbacks: {
                                                                allowHTML5AutoPlayOnAndroid: true
                                                            },
                                                        });
                                                    }

                                                });
                                            } // End of RevInitScript
                                            var once_revslider11 = false;
                                            if (document.readyState === "loading") {
                                                document.addEventListener('readystatechange', function () {
                                                    if ((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider11) {
                                                        once_revslider11 = true;
                                                        revinit_revslider11();
                                                    }
                                                });
                                            } else {
                                                once_revslider11 = true;
                                                revinit_revslider11();
                                            }
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape(".tparrows.qodef-navigation-light%20%7B%0A%20%20%20%20background%3A%20transparent%3B%0A%20%20%09width%3A%2034px%3B%0A%20%09height%3A%2020px%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Abefore%20%7B%0A%20%20%20%20font-size%3A%200%3B%0A%20%20%20%20color%3A%20transparent%3B%0A%20%20%20%20line-height%3A%200%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%20svg%20line%20%7B%0A%20%20%09stroke-dasharray%3A%2033%3B%0A%20%20%09stroke-dashoffset%3A%200%3B%0A%20%20%09transition%3A%20stroke-dashoffset%20.6s%20cubic-bezier%280.55%2C%200.46%2C%200.05%2C%200.65%29%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%20svg%20polyline%20%7B%0A%20%20%09stroke-dasharray%3A%2027%3B%0A%20%20%09stroke-dashoffset%3A%200%3B%0A%20%20%09transition%3A%20stroke-dashoffset%20.6s%20cubic-bezier%280.55%2C%200.46%2C%200.05%2C%200.65%29%20.2s%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Ahover%20svg%20line%20%7B%0A%20%20%09stroke-dashoffset%3A%2066%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light%3Ahover%20svg%20polyline%20%7B%0A%20%20%09stroke-dashoffset%3A%2054%3B%0A%7D%0A%0A.tparrows.qodef-navigation-light.tp-leftarrow%20svg%20%7B%0A%09transform%3A%20rotate%28180deg%29%3B%0A%7D%0A.qodef-bullets-light%20.tp-bullet%20%7B%0A%09border-radius%3A%2050%25%3B%0A%20%20%09background-color%3A%20%23fff%3B%0A%20%20%09width%3A%205px%3B%0A%20%20%09height%3A%205px%3B%0A%20%20%09transition%3A%20transform%20.2s%20ease-out%3B%0A%7D%0A%0A.qodef-bullets-light%20.tp-bullet%3Ahover%20%7B%0A%20%09transform%3A%20scale%281.8%29%3B%0A%7D%0A%0A.qodef-bullets-light%20.tp-bullet.selected%20%7B%0A%09transform%3A%20scale%281.8%29%3B%0A%7D%0A%0A.qodef-bullets-light.qodef--dark%20.tp-bullet%7B%0A%09background-color%3A%20%23232323%3B%0A%7D%0A");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                    </rs-module-wrap>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="productos"
         class="hidden qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-d76c80b qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
         data-id="d76c80b"
         data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fba915a"
                 data-id="fba915a" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-30e2a83 elementor-widget__width-initial elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="30e2a83" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 14px 0px ">
<span class="qodef-caption-line qodef--left"></span> Original recipe <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h1 class="qodef-m-title"
                                        style="color: #232323">
                                        refined whiskey </h1>
                                    <p class="qodef-m-text"
                                       style="margin: 13px;font-size: 17px">Lorem
                                        ipsum dolor sit amet, a mea epicuri
                                        vituperator bus ea, ea nam movet
                                        complectitur.</p>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-56dcf22 elementor-widget elementor-widget-singlemalt_core_item_showcase"
                             data-id="56dcf22" data-element_type="widget"
                             data-widget_type="singlemalt_core_item_showcase.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-item-showcase qodef-layout--list"
                                     style="background: url(themes/singlemalt/wp-content/uploads/2020/09/h1-item-showcase-background-img-1.png);background-repeat: no-repeat;background-position: center calc(50% + 60px)">
                                    <div class="qodef-m-items qodef--left">
                                        <div class="qodef-m-item qodef-e">
                                            <div class="qodef-e-item-image">
                                                <img width="98" height="142"
                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-1.png"
                                                     class="attachment-full size-full"
                                                     alt="jj"/></div>
                                            <h3 class="qodef-e-title"
                                                style="color: #232323">
                                                malting </h3>
                                            <p class="qodef-e-text"
                                               style="color: #191919">Lorem ipsum
                                                dolor sit amet con tetur adi isicing
                                                elit sed do eius od tempor in
                                                incide</p>
                                        </div>
                                        <div class="qodef-m-item qodef-e">
                                            <div class="qodef-e-item-image">
                                                <img width="96" height="168"
                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-2.png"
                                                     class="attachment-full size-full"
                                                     alt="jj"/></div>
                                            <h3 class="qodef-e-title"
                                                style="color: #232323">
                                                mashing </h3>
                                            <p class="qodef-e-text"
                                               style="color: #191919">Lorem ipsum
                                                dolor sit amet con tetur adi isicing
                                                elit sed do eius od tempor in
                                                incide</p>
                                        </div>
                                    </div>
                                    <div class="qodef-m-image"
                                         style="margin-top: 16px">
                                        <img width="238" height="824"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-showcase-item-img-1.png"
                                             class="attachment-full size-full"
                                             alt="jj"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-showcase-item-img-1.png 238w, wp-content/uploads/2020/07/h1-showcase-item-img-1-87x300.png 87w"
                                             sizes="(max-width: 238px) 100vw, 238px"/>
                                    </div>
                                    <div class="qodef-m-items qodef--right">
                                        <div class="qodef-m-item qodef-e">
                                            <div class="qodef-e-item-image">
                                                <img width="91" height="134"
                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-3.png"
                                                     class="attachment-full size-full"
                                                     alt="jj"/></div>
                                            <h3 class="qodef-e-title"
                                                style="color: #232323">
                                                distillation </h3>
                                            <p class="qodef-e-text">Lorem ipsum
                                                dolor sit amet con tetur adi isicing
                                                elit sed do eius od tempor in
                                                incide</p>
                                        </div>
                                        <div class="qodef-m-item qodef-e">
                                            <div class="qodef-e-item-image">
                                                <img width="82" height="137"
                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-4.png"
                                                     class="attachment-full size-full"
                                                     alt="aa"/></div>
                                            <h3 class="qodef-e-title"
                                                style="color: #232323">
                                                maturation </h3>
                                            <p class="qodef-e-text">Lorem ipsum
                                                dolor sit amet con tetur adi isicing
                                                elit sed do eius od tempor in
                                                incide</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden elementor-section elementor-top-section elementor-element elementor-element-9618a17 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
        data-id="9618a17" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-82b3c30"
                 data-id="82b3c30" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-8069452 elementor-widget elementor-widget-singlemalt_core_video_button"
                             data-id="8069452" data-element_type="widget"
                             data-widget_type="singlemalt_core_video_button.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-video-button qodef--has-img qodef--advanced-layout  qodef-appear-animation">
                                    <div class="qodef-m-image">
                                        <img width="1300" height="846"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1.jpg"
                                             class="attachment-full size-full"
                                             alt="aa"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1.jpg 1300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1-600x390.jpg 600w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1-300x195.jpg 300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1-1024x666.jpg 1024w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-1-768x500.jpg 768w"
                                             sizes="(max-width: 1300px) 100vw, 1300px"/>
                                    </div>
                                    <a itemprop="url"
                                       class="qodef-m-play qodef-magnific-popup qodef-popup-item"
                                       style="color: #FFFFFF;font-size: 45px"
                                       href="https://vimeo.com/214149450"
                                       data-type="iframe">
<span class="qodef-m-play-inner">
<svg xmlns="http://www.w3.org/2000/svg" width="42.578" height="42.547">
<circle fill="none" stroke="#BC9D72" stroke-width="1.4" stroke-miterlimit="10" cx="21.297" cy="21.278" r="20.5"/>
<path fill="#FFF" d="M18.625 28.278v-14l8 7z"/>
</svg>
</span>
                                    </a>
                                    <div class="qodef-m-text">
                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  ">
                                                                                <span class="qodef-m-caption"
                                                                                      style="margin: 0 0 9px"> <span
                                                                                            class="qodef-caption-line qodef--left"></span> Campaigns </span>
                                            <h2 class="qodef-m-title"
                                                style="color: #fff">
                                                one for all campaign </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-936edd0"
                 data-id="936edd0" data-element_type="column"
                 data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-a79b02a elementor-widget elementor-widget-singlemalt_core_video_button"
                             data-id="a79b02a" data-element_type="widget"
                             data-widget_type="singlemalt_core_video_button.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-video-button qodef--has-img qodef--advanced-layout  qodef-appear-animation">
                                    <div class="qodef-m-image">
                                        <img width="1300" height="846"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2.jpg"
                                             class="attachment-full size-full"
                                             alt="aa"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2.jpg 1300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2-600x390.jpg 600w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2-300x195.jpg 300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2-1024x666.jpg 1024w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-2-768x500.jpg 768w"
                                             sizes="(max-width: 1300px) 100vw, 1300px"/>
                                    </div>
                                    <a itemprop="url"
                                       class="qodef-m-play qodef-magnific-popup qodef-popup-item"
                                       style="color: #FFFFFF;font-size: 45px"
                                       href="https://vimeo.com/128223326"
                                       data-type="iframe">
<span class="qodef-m-play-inner">
<svg xmlns="http://www.w3.org/2000/svg" width="42.578" height="42.547">
<circle fill="none" stroke="#BC9D72" stroke-width="1.4" stroke-miterlimit="10" cx="21.297" cy="21.278" r="20.5"/>
<path fill="#FFF" d="M18.625 28.278v-14l8 7z"/>
</svg>
</span>
                                    </a>
                                    <div class="qodef-m-text">
                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  ">
                                                                                <span class="qodef-m-caption"
                                                                                      style="margin: 0 0 9px"> <span
                                                                                            class="qodef-caption-line qodef--left"></span> Our blog </span>
                                            <h2 class="qodef-m-title"
                                                style="color: #fff">
                                                recipe of the week </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden elementor-section elementor-top-section elementor-element elementor-element-25af458 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
        data-id="25af458" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-518e408"
                 data-id="518e408" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-c767f76 elementor-widget elementor-widget-singlemalt_core_video_button"
                             data-id="c767f76" data-element_type="widget"
                             data-widget_type="singlemalt_core_video_button.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-video-button qodef--has-img qodef--advanced-layout  qodef-appear-animation">
                                    <div class="qodef-m-image">
                                        <img width="1300" height="846"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3.jpg"
                                             class="attachment-full size-full"
                                             alt="jj"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3.jpg 1300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3-600x390.jpg 600w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3-300x195.jpg 300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3-1024x666.jpg 1024w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-3-768x500.jpg 768w"
                                             sizes="(max-width: 1300px) 100vw, 1300px"/>
                                    </div>
                                    <a itemprop="url"
                                       class="qodef-m-play qodef-magnific-popup qodef-popup-item"
                                       style="color: #FFFFFF;font-size: 45px"
                                       href="https://vimeo.com/87070469"
                                       data-type="iframe">
<span class="qodef-m-play-inner">
<svg xmlns="http://www.w3.org/2000/svg" width="42.578" height="42.547">
<circle fill="none" stroke="#BC9D72" stroke-width="1.4" stroke-miterlimit="10" cx="21.297" cy="21.278" r="20.5"/>
<path fill="#FFF" d="M18.625 28.278v-14l8 7z"/>
</svg>
</span>
                                    </a>
                                    <div class="qodef-m-text">
                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  ">
                                                                                <span class="qodef-m-caption"
                                                                                      style="margin: 0 0 9px"> <span
                                                                                            class="qodef-caption-line qodef--left"></span> Real stories </span>
                                            <h2 class="qodef-m-title"
                                                style="color: #fff">
                                                wood and flame </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2b48f57"
                 data-id="2b48f57" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-76f66b3 elementor-widget elementor-widget-singlemalt_core_video_button"
                             data-id="76f66b3" data-element_type="widget"
                             data-widget_type="singlemalt_core_video_button.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-video-button qodef--has-img qodef--advanced-layout  qodef-appear-animation">
                                    <div class="qodef-m-image">
                                        <img width="1300" height="846"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4.jpg"
                                             class="attachment-full size-full"
                                             alt="aa"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4.jpg 1300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4-600x390.jpg 600w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4-300x195.jpg 300w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4-1024x666.jpg 1024w, themes/singlemalt/wp-content/uploads/2020/07/h1-video-button-img-4-768x500.jpg 768w"
                                             sizes="(max-width: 1300px) 100vw, 1300px"/>
                                    </div>
                                    <a itemprop="url"
                                       class="qodef-m-play qodef-magnific-popup qodef-popup-item"
                                       style="color: #FFFFFF;font-size: 45px"
                                       href="https://vimeo.com/252549165"
                                       data-type="iframe">
<span class="qodef-m-play-inner">
<svg xmlns="http://www.w3.org/2000/svg" width="42.578" height="42.547">
<circle fill="none" stroke="#BC9D72" stroke-width="1.4" stroke-miterlimit="10" cx="21.297" cy="21.278" r="20.5"/>
<path fill="#FFF" d="M18.625 28.278v-14l8 7z"/>
</svg>
</span>
                                    </a>
                                    <div class="qodef-m-text">
                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  ">
                                                                                <span class="qodef-m-caption"
                                                                                      style="margin: 0 0 9px"> <span
                                                                                            class="qodef-caption-line qodef--left"></span> Promotions </span>
                                            <h2 class="qodef-m-title"
                                                style="color: #fff">
                                                holiday time </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden qodef-background-text-section qodef-background-images-section elementor-section elementor-top-section elementor-element elementor-element-6d16a41 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
        data-background-text="{&quot;text&quot;:&quot;the journey&quot;,&quot;margin&quot;:&quot;87px 0px 0px 0px&quot;,&quot;margin_tablet&quot;:&quot;105px 0px 0px 0px&quot;,&quot;margin_phone&quot;:&quot;60px 0px 0px 0px&quot;,&quot;font_size&quot;:&quot;140px&quot;,&quot;color&quot;:&quot;#FAF7F2&quot;}"
        data-background-images="{&quot;image_left&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;image_right&quot;:{&quot;url&quot;:&quot;https:\/\/singlemalt.qodeinteractive.com\/wp-content\/uploads\/2020\/07\/h1-row-background-img-1.png&quot;,&quot;id&quot;:196},&quot;svg_left&quot;:&quot;&quot;,&quot;svg_right&quot;:&quot;&quot;,&quot;images_top&quot;:&quot;133px&quot;}"
        data-id="6d16a41" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-695eb33"
                 data-id="695eb33" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-0379940 elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="0379940" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 14px 0px ">
<span class="qodef-caption-line qodef--left"></span> The journey <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h1 class="qodef-m-title"
                                        style="color: #232323">
                                        a century of tradition </h1>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-db27261 elementor-widget elementor-widget-singlemalt_core_roadmap"
                             data-id="db27261" data-element_type="widget"
                             data-widget_type="singlemalt_core_roadmap.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-roadmap "
                                     data-unique-id="1">
                                    <div class="qodef-m-line"></div>
                                    <div class="qodef-m-inner clearfix">
                                        <div class="qodef-roadmap-item qodef-e qodef-order--odd">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    two brothers </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1897 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/07/h1-roadmap-item-img-1.png"
     class="attachment-full size-full" alt="jj"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--even">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    the first barrel </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1902 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/07/h1-roadmap-item-img-2.png"
     class="attachment-full size-full" alt="aa"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--odd">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    the old house </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate.
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1920 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/07/h1-roadmap-item-img-3.png"
     class="attachment-full size-full" alt="jj"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--even">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    one dream </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1936 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/07/h1-roadmap-item-img-4.png"
     class="attachment-full size-full" alt="aa"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--odd">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    family tradition </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1965 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/07/h1-roadmap-item-img-5.png"
     class="attachment-full size-full" alt="jj"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--even">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    the distillery </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1970 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/09/h1-roadmap-item-img-6.png"
     class="attachment-full size-full" alt=""/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--odd">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    the first store </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1983 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/09/h1-roadmap-item-img-7.png"
     class="attachment-full size-full" alt="jj"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--even">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    new bottle </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    1990 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/09/h1-roadmap-item-img-8.png"
     class="attachment-full size-full" alt=""/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--odd">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    partnership </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    2002 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/09/h1-roadmap-item-img-9.png"
     class="attachment-full size-full" alt="jj"/> </span>
                                            </div>
                                        </div>
                                        <div class="qodef-roadmap-item qodef-e qodef-order--even">
                                            <div class="qodef-e-content-holder">
                                                <h4 class="qodef-e-content-title">
                                                    donations </h4>
                                                <div class="qodef-e-content">
                                                    Novum partem gloriatur has ne,
                                                    falli melius expetendis ad eam.
                                                    Ut per oblique vulputate
                                                </div>
                                            </div>
                                            <div class="qodef-e-circle-holder">
                                                <div class="qodef-e-before-circle"></div>
                                                <div class="qodef-e-circle"></div>
                                                <p class="qodef-e-year">
                                                    2014 </p>
                                                <div class="qodef-e-after-circle"></div>
                                            </div>
                                            <div class="qodef-e-stage-image-holder">
                                                                                        <span class="qodef-e-stage-title">
<img width="269" height="199" src="themes/singlemalt/wp-content/uploads/2020/09/h1-roadmap-item-img-10.png"
     class="attachment-full size-full" alt=""/> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="qodef-roadmap-navigation">
                                                                            <span class="qodef-navigation-item qodef-navigation-item-1 qodef--left">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="33.85px" height="19.958px" viewBox="0 0 33.85 19.958" enable-background="new 0 0 33.85 19.958"
     xml:space="preserve">
<line fill="none" stroke-width="1.4" stroke-miterlimit="10" x1="0" y1="9.979" x2="33" y2="9.979"/>
<polyline fill="none" stroke-width="1.4" stroke-miterlimit="10" points="23.376,0.495 32.859,9.979 23.376,19.463 "/>
</svg>
</span>
                                    <span class="qodef-navigation-item qodef-navigation-item-1 qodef--right qodef--right-1">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="33.85px" height="19.958px" viewBox="0 0 33.85 19.958" enable-background="new 0 0 33.85 19.958"
     xml:space="preserve">
<line fill="none" stroke-width="1.4" stroke-miterlimit="10" x1="0" y1="9.979" x2="33" y2="9.979"/>
<polyline fill="none" stroke-width="1.4" stroke-miterlimit="10" points="23.376,0.495 32.859,9.979 	23.376,19.463 "/>
</svg>
</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden elementor-section elementor-top-section elementor-element elementor-element-5542cf9 elementor-section-full_width qodfe-reduce-padding elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
        data-id="5542cf9"
        data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-2010981"
                 data-id="2010981" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-b98a399 elementor-widget elementor-widget-singlemalt_core_banner"
                             data-id="b98a399" data-element_type="widget"
                             data-widget_type="singlemalt_core_banner.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-banner qodef-layout--link-button qodef-content-position--middle qodef-text-align--center">
                                    <div class="qodef-m-image">
                                        <img width="800" height="1081"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-1.jpg"
                                             class="attachment-full size-full"
                                             alt="jj"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-1.jpg 800w, wp-content/uploads/2020/07/h1-banner-img-1-600x811.jpg 600w, wp-content/uploads/2020/07/h1-banner-img-1-222x300.jpg 222w, wp-content/uploads/2020/07/h1-banner-img-1-758x1024.jpg 758w, wp-content/uploads/2020/07/h1-banner-img-1-768x1038.jpg 768w"
                                             sizes="(max-width: 800px) 100vw, 800px"/>
                                    </div>
                                    <div class="qodef-m-content">
                                        <div class="qodef-m-content-inner">
                                            <div class="qodef-m-caption">
                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  "></div>
                                            </div>
                                            <h2 class="qodef-m-title"
                                                style="padding: 0% 18%;color: #FFFFFF">
                                                family business legacy </h2>
                                            <div class="qodef-m-button">
                                                <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--textual  qodef-html--link qodef-skin--default qodef-skin--default"
                                                   href="our-history/"
                                                   target="_self"
                                                   style="color: #FFFFFF;font-size: 12px">
                                                                                        <span class="qodef-m-text"> <span
                                                                                                    class="qodef-button-arrow-back"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> <span
                                                                                                    class="qodef-m-text-inner">view more</span> <span
                                                                                                    class="qodef-button-arrow-front"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7c0eff9"
                 data-id="7c0eff9" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-0c182f6 elementor-widget elementor-widget-singlemalt_core_banner"
                             data-id="0c182f6" data-element_type="widget"
                             data-widget_type="singlemalt_core_banner.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-banner qodef-layout--link-button qodef-content-position--middle qodef-text-align--center">
                                    <div class="qodef-m-image">
                                        <img width="800" height="1081"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-2.jpg"
                                             class="attachment-full size-full"
                                             alt="aa"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-2.jpg 800w, wp-content/uploads/2020/07/h1-banner-img-2-600x811.jpg 600w, wp-content/uploads/2020/07/h1-banner-img-2-222x300.jpg 222w, wp-content/uploads/2020/07/h1-banner-img-2-758x1024.jpg 758w, wp-content/uploads/2020/07/h1-banner-img-2-768x1038.jpg 768w"
                                             sizes="(max-width: 800px) 100vw, 800px"/>
                                    </div>
                                    <div class="qodef-m-content">
                                        <div class="qodef-m-content-inner">
                                            <div class="qodef-m-caption">
                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  "></div>
                                            </div>
                                            <h2 class="qodef-m-title"
                                                style="padding: 0% 18%;color: #FFFFFF">
                                                distribution worldwide </h2>
                                            <div class="qodef-m-button">
                                                <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--textual  qodef-html--link qodef-skin--default qodef-skin--default"
                                                   href="contact-us/" target="_self"
                                                   style="color: #FFFFFF;font-size: 12px">
                                                                                        <span class="qodef-m-text"> <span
                                                                                                    class="qodef-button-arrow-back"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> <span
                                                                                                    class="qodef-m-text-inner">view more</span> <span
                                                                                                    class="qodef-button-arrow-front"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-f3f0d42"
                 data-id="f3f0d42" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-b02786c elementor-widget elementor-widget-singlemalt_core_banner"
                             data-id="b02786c" data-element_type="widget"
                             data-widget_type="singlemalt_core_banner.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-banner qodef-layout--link-button qodef-content-position--middle qodef-text-align--center">
                                    <div class="qodef-m-image">
                                        <img width="800" height="1081"
                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-3.jpg"
                                             class="attachment-full size-full"
                                             alt="aa"
                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-banner-img-3.jpg 800w, wp-content/uploads/2020/07/h1-banner-img-3-600x811.jpg 600w, wp-content/uploads/2020/07/h1-banner-img-3-222x300.jpg 222w, wp-content/uploads/2020/07/h1-banner-img-3-758x1024.jpg 758w, wp-content/uploads/2020/07/h1-banner-img-3-768x1038.jpg 768w"
                                             sizes="(max-width: 800px) 100vw, 800px"/>
                                    </div>
                                    <div class="qodef-m-content">
                                        <div class="qodef-m-content-inner">
                                            <div class="qodef-m-caption">
                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  "></div>
                                            </div>
                                            <h2 class="qodef-m-title"
                                                style="padding: 0% 18%;color: #FFFFFF">
                                                the best recipe selection </h2>
                                            <div class="qodef-m-button">
                                                <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--textual  qodef-html--link qodef-skin--default qodef-skin--default"
                                                   href="no-sidebar/" target="_self"
                                                   style="color: #FFFFFF;font-size: 12px">
                                                                                        <span class="qodef-m-text"> <span
                                                                                                    class="qodef-button-arrow-back"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> <span
                                                                                                    class="qodef-m-text-inner">view more</span> <span
                                                                                                    class="qodef-button-arrow-front"> <svg
                                                                                                        version="1.1"
                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                        x="0px" y="0px"
                                                                                                        width="14"
                                                                                                        height="7"
                                                                                                        viewBox="0 0 14.26 7.012"
                                                                                                        enable-background="new 0 0 14.26 7.012"
                                                                                                        xml:space="preserve"> <line
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            x1="0"
                                                                                                            y1="3.506"
                                                                                                            x2="13.553"
                                                                                                            y2="3.506"/> <polyline
                                                                                                            fill="none"
                                                                                                            stroke="#232323"
                                                                                                            stroke-miterlimit="10"
                                                                                                            points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden qodef-row-frame qodef-background-text-section elementor-section elementor-top-section elementor-element elementor-element-9dd91b1 elementor-section-full_width qodef-elementor-content-grid qodef-reduce-bt-fs elementor-section-height-default elementor-section-height-default"
        data-background-text="{&quot;text&quot;:&quot;golden numbers&quot;,&quot;margin&quot;:&quot;110px 0px 0px 0px&quot;,&quot;margin_tablet&quot;:&quot;95px 0px 0px 5px&quot;,&quot;margin_phone&quot;:&quot;60px 0px 0px 0px&quot;,&quot;font_size&quot;:&quot;140px&quot;,&quot;color&quot;:&quot;#FAF7F2&quot;}"
        data-id="9dd91b1" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-163509a"
                 data-id="163509a" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <section
                                class="elementor-section elementor-inner-section elementor-element elementor-element-7de4c6e elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                data-id="7de4c6e" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b65c0c9"
                                         data-id="b65c0c9"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-f393c55 elementor-widget__width-initial elementor-widget elementor-widget-singlemalt_core_section_title"
                                                     data-id="f393c55"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_section_title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                                                    <span class="qodef-m-caption"
                                                                                                          style="margin: 0px 0px 11px 0px">
<span class="qodef-caption-line qodef--left"></span> Our legacy <span class="qodef-caption-line qodef--right"></span>
                                                                                                    </span>
                                                            <h2 class="qodef-m-title">
                                                                A long journey </h2>
                                                            <p class="qodef-m-text">
                                                                Lorem ipsum dolor
                                                                sit amet consec
                                                                tetur se isicin
                                                                elit, sed do eiusm
                                                                od tempor inset
                                                                incia diduni ut
                                                                labore et sins
                                                                dolore sine magnas
                                                                aliqua uta eniad
                                                                sen</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-cda32df elementor-widget elementor-widget-spacer"
                                                     data-id="cda32df"
                                                     data-element_type="widget"
                                                     data-widget_type="spacer.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-spacer">
                                                            <div class="elementor-spacer-inner"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-fd7e0f8 elementor-widget__width-auto elementor-widget elementor-widget-singlemalt_core_button"
                                                     data-id="fd7e0f8"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_button.default">
                                                    <div class="elementor-widget-container">
                                                        <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                           href="about-us/"
                                                           target="_self">
<span class="qodef-m-text">
discover
<span class="qodef-button-arrow-svg">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="14" height="7" viewBox="0 0 14.26 7.012" enable-background="new 0 0 14.26 7.012" xml:space="preserve">
<line fill="none" stroke-miterlimit="10" x1="0" y1="3.506" x2="13.553" y2="3.506"/>
<polyline fill="none" stroke-miterlimit="10" points="10.4,0.354 13.553,3.507 10.4,6.659 "/>
</svg>
</span>
</span>
                                                        </a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-20ad6e3"
                                         data-id="20ad6e3"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-c09cfed elementor-widget elementor-widget-image"
                                                     data-id="c09cfed"
                                                     data-element_type="widget"
                                                     data-widget_type="image.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-image">
                                                            <img width="486"
                                                                 height="620"
                                                                 src="themes/singlemalt/wp-content/uploads/2020/07/h1-slide2-img-1.png"
                                                                 class="attachment-full size-full"
                                                                 alt="aa"
                                                                 srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-slide2-img-1.png 486w, wp-content/uploads/2020/07/h1-slide2-img-1-235x300.png 235w"
                                                                 sizes="(max-width: 486px) 100vw, 486px"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5a3b918"
                                         data-id="5a3b918"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-0ef8240 elementor-widget__width-auto elementor-widget elementor-widget-heading"
                                                     data-id="0ef8240"
                                                     data-element_type="widget"
                                                     data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <h3 class="elementor-heading-title elementor-size-default">
                                                            The texture</h3>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-2ac48c8 elementor-widget elementor-widget-spacer"
                                                     data-id="2ac48c8"
                                                     data-element_type="widget"
                                                     data-widget_type="spacer.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-spacer">
                                                            <div class="elementor-spacer-inner"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-3b30a73 elementor-widget__width-initial elementor-widget-mobile__width-auto elementor-widget elementor-widget-singlemalt_core_intensity"
                                                     data-id="3b30a73"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_intensity.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-intensity qodef-intensity--4">
                                                            <div class="qodef-intensity-wrapper">
                                                                                                        <span class="qodef-m-text">
Bone dry </span>
                                                                <span class="qodef-m-intensity">
<span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-5db182f elementor-widget__width-initial elementor-widget elementor-widget-singlemalt_core_intensity"
                                                     data-id="5db182f"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_intensity.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-intensity qodef-intensity--4">
                                                            <div class="qodef-intensity-wrapper">
                                                                                                        <span class="qodef-m-text">
Dry </span>
                                                                <span class="qodef-m-intensity">
<span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-3d69481 elementor-widget elementor-widget-spacer"
                                                     data-id="3d69481"
                                                     data-element_type="widget"
                                                     data-widget_type="spacer.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-spacer">
                                                            <div class="elementor-spacer-inner"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-f77bfb0 elementor-widget__width-initial elementor-widget elementor-widget-singlemalt_core_intensity"
                                                     data-id="f77bfb0"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_intensity.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-intensity qodef-intensity--4">
                                                            <div class="qodef-intensity-wrapper">
                                                                                                        <span class="qodef-m-text">
Sweet </span>
                                                                <span class="qodef-m-intensity">
<span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-fda7f20 elementor-widget__width-initial elementor-widget elementor-widget-singlemalt_core_intensity"
                                                     data-id="fda7f20"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_intensity.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-intensity qodef-intensity--4">
                                                            <div class="qodef-intensity-wrapper">
                                                                                                        <span class="qodef-m-text">
Sour </span>
                                                                <span class="qodef-m-intensity">
<span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        <span class="qodef-intensity-circle"></span>
                                                                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section
                                class="elementor-section elementor-inner-section elementor-element elementor-element-903baae elementor-section-full_width qodef-elementor-content-grid elementor-section-content-middle elementor-section-height-default elementor-section-height-default"
                                data-id="903baae" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-82cf321"
                                         data-id="82cf321"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-b7de81a elementor-widget__width-initial elementor-widget-tablet__width-initial elementor-widget elementor-widget-singlemalt_core_round_label"
                                                     data-id="b7de81a"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_round_label.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-round-label">
                                                            <div class="qodef-round-label-wrapper">
                                                                                                        <span class="qodef-m-number">
100% </span>
                                                                <span class="qodef-m-label">
Available on stock </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-a5cd9f1 elementor-widget__width-initial elementor-widget-tablet__width-initial elementor-widget elementor-widget-singlemalt_core_round_label"
                                                     data-id="a5cd9f1"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_round_label.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-round-label">
                                                            <div class="qodef-round-label-wrapper">
                                                                                                        <span class="qodef-m-number"
                                                                                                              style="padding:  21px 19px 17px 20px">
5% </span>
                                                                <span class="qodef-m-label">
Alcohol by volume </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-32197ba elementor-widget__width-auto elementor-widget-tablet__width-initial elementor-widget elementor-widget-singlemalt_core_round_label"
                                                     data-id="32197ba"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_round_label.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-round-label">
                                                            <div class="qodef-round-label-wrapper">
                                                                                                        <span class="qodef-m-number"
                                                                                                              style="padding:  21px 23px 17px">
10 </span>
                                                                <span class="qodef-m-label">
Sold in packages of 10 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="height: 390px"
         class="hidden elementor-section elementor-top-section elementor-element elementor-element-e10afc9 elementor-section-height-min-height qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
         data-id="e10afc9" data-element_type="section"
         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-a257a60"
                 data-id="a257a60" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-514b6be elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="514b6be" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--left  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="font-size: 20px">
 <span class="qodef-caption-line qodef--left"></span> Subscribe </span>
                                    <h2 class="qodef-m-title"
                                        style="color: #FFFFFF;margin: -5px 0px 0px 0px">
                                        SIGN UP FOR OUR NEWSLETTER NOW </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-83af268"
                 data-id="83af268" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-aa37d2d elementor-widget__width-auto elementor-widget elementor-widget-wp-widget-singlemalt_core_contact_form_7"
                             data-id="aa37d2d" data-element_type="widget"
                             data-widget_type="wp-widget-singlemalt_core_contact_form_7.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-contact-form-7">
                                    <div role="form" class="wpcf7"
                                         id="wpcf7-f646-p17-o1" lang="en-US"
                                         dir="ltr">
                                        <div class="screen-reader-response"
                                             role="alert" aria-live="polite"></div>
                                        <form action="/#wpcf7-f646-p17-o1"
                                              method="post" class="wpcf7-form init"
                                              novalidate="novalidate">
                                            <div style="display: none;">
                                                <input type="hidden" name="_wpcf7"
                                                       value="646"/>
                                                <input type="hidden"
                                                       name="_wpcf7_version"
                                                       value="5.2.2"/>
                                                <input type="hidden"
                                                       name="_wpcf7_locale"
                                                       value="en_US"/>
                                                <input type="hidden"
                                                       name="_wpcf7_unit_tag"
                                                       value="wpcf7-f646-p17-o1"/>
                                                <input type="hidden"
                                                       name="_wpcf7_container_post"
                                                       value="17"/>
                                                <input type="hidden"
                                                       name="_wpcf7_posted_data_hash"
                                                       value=""/>
                                            </div>
                                            <div class="qodef-newsletter-form">
                                                <div class="qodef-newsletter-form-text">
                                                    <label><span
                                                                class="wpcf7-form-control-wrap your-mail"><input
                                                                    type="email"
                                                                    name="your-mail"
                                                                    value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email"
                                                                    aria-invalid="false"
                                                                    placeholder="Your mail"/></span></label>
                                                </div>
                                                <div class="qodef-newsletter-form-submit">
                                                    <button type="submit"
                                                            class="wpcf7-form-control wpcf7-submit qodef-button qodef-size--normal qodef-type--filled qodef-m">
                                                        <span class="qodef-m-text"><?= System::translate('subscribe') ?></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="wpcf7-response-output"
                                                 role="alert"
                                                 aria-hidden="true"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-f237017 qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
        data-id="f237017"
        data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-276da2f"
                 data-id="276da2f" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-72991f4 elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="72991f4" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 14px 0px ">
<span class="qodef-caption-line qodef--left"></span> The selection <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h1 class="qodef-m-title"
                                        style="color: #232323">
                                        the original whiskey </h1>
                                </div>
                            </div>
                        </div>
                        <section
                                class="elementor-section elementor-inner-section elementor-element elementor-element-2a2f3ef qodef-elementor-content-grid qodef-remove-responsive-borders elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="2a2f3ef" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-91fb2d9"
                                         data-id="91fb2d9"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-de4d502 elementor-widget elementor-widget-singlemalt_core_pricing_table"
                                                     data-id="de4d502"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_pricing_table.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-pricing-table qodef-layout--standard qodef-status--regular"
                                                             style="padding: 0px 30px">
                                                            <div class="qodef-m-inner">
                                                                <h4 class="qodef-m-title">
                                                                    <span>bourbon</span>
                                                                </h4>
                                                                <div class="qodef-m-content">
                                                                    <p>Qui nemore
                                                                        molestie et
                                                                        civibus
                                                                        probatus</p>
                                                                </div>
                                                                <div class="qodef-m-image">
                                                                    <img width="346"
                                                                         height="332"
                                                                         src="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img1.png"
                                                                         class="attachment-full size-full"
                                                                         alt="jj"
                                                                         srcset="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img1.png 346w, wp-content/uploads/2020/09/h1-pricing-img1-300x288.png 300w"
                                                                         sizes="(max-width: 346px) 100vw, 346px"/>
                                                                </div>
                                                                <p class="qodef-m-label">
                                                                    <span>Starting at</span>
                                                                </p>
                                                                <div class="qodef-m-price">
                                                                    <div class="qodef-m-price-wrapper qodef-h1"
                                                                         style="color: #232323">
                                                                                                            <span class="qodef-m-price-currency"
                                                                                                                  style="color: #232323">$</span>
                                                                        <span class="qodef-m-price-value">80</span>
                                                                    </div>
                                                                </div>
                                                                <div class="qodef-m-button clear">
                                                                    <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                                       href="product-category/liqueur/"
                                                                       target="_self"
                                                                       data-hover-color="#FFFFFF"
                                                                       data-hover-background-color="#BC9D72"
                                                                       style="color: #232323;font-size: 12px">
                                                                                                            <span class="qodef-m-text"> view more <span
                                                                                                                        class="qodef-button-arrow-svg"> <svg
                                                                                                                            version="1.1"
                                                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                                            x="0px"
                                                                                                                            y="0px"
                                                                                                                            width="14"
                                                                                                                            height="7"
                                                                                                                            viewBox="0 0 14.26 7.012"
                                                                                                                            enable-background="new 0 0 14.26 7.012"
                                                                                                                            xml:space="preserve"> <line
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                x1="0"
                                                                                                                                y1="3.506"
                                                                                                                                x2="13.553"
                                                                                                                                y2="3.506"/> <polyline
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5d325c1"
                                         data-id="5d325c1"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-689c55b elementor-widget elementor-widget-singlemalt_core_pricing_table"
                                                     data-id="689c55b"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_pricing_table.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-pricing-table qodef-layout--standard qodef-status--regular"
                                                             style="padding: 0px 30px">
                                                            <div class="qodef-m-inner">
                                                                <h4 class="qodef-m-title">
                                                                    <span>rye islay</span>
                                                                </h4>
                                                                <div class="qodef-m-content">
                                                                    <p>Qui nemore
                                                                        molestie et
                                                                        civibus
                                                                        probatus</p>
                                                                </div>
                                                                <div class="qodef-m-image">
                                                                    <img width="346"
                                                                         height="332"
                                                                         src="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img2.png"
                                                                         class="attachment-full size-full"
                                                                         alt="jj"
                                                                         srcset="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img2.png 346w, wp-content/uploads/2020/09/h1-pricing-img2-300x288.png 300w"
                                                                         sizes="(max-width: 346px) 100vw, 346px"/>
                                                                </div>
                                                                <p class="qodef-m-label">
                                                                    <span>Starting at</span>
                                                                </p>
                                                                <div class="qodef-m-price">
                                                                    <div class="qodef-m-price-wrapper qodef-h1"
                                                                         style="color: #232323">
                                                                                                            <span class="qodef-m-price-currency"
                                                                                                                  style="color: #232323">$</span>
                                                                        <span class="qodef-m-price-value">59</span>
                                                                    </div>
                                                                </div>
                                                                <div class="qodef-m-button clear">
                                                                    <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                                       href="product-category/whiskey/"
                                                                       target="_self"
                                                                       data-hover-color="#FFFFFF"
                                                                       data-hover-background-color="#BC9D72"
                                                                       style="color: #232323;font-size: 12px">
                                                                                                            <span class="qodef-m-text"> view more <span
                                                                                                                        class="qodef-button-arrow-svg"> <svg
                                                                                                                            version="1.1"
                                                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                                            x="0px"
                                                                                                                            y="0px"
                                                                                                                            width="14"
                                                                                                                            height="7"
                                                                                                                            viewBox="0 0 14.26 7.012"
                                                                                                                            enable-background="new 0 0 14.26 7.012"
                                                                                                                            xml:space="preserve"> <line
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                x1="0"
                                                                                                                                y1="3.506"
                                                                                                                                x2="13.553"
                                                                                                                                y2="3.506"/> <polyline
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-067e286"
                                         data-id="067e286"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-a0af78e elementor-widget elementor-widget-singlemalt_core_pricing_table"
                                                     data-id="a0af78e"
                                                     data-element_type="widget"
                                                     data-widget_type="singlemalt_core_pricing_table.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="qodef-shortcode qodef-m  qodef-pricing-table qodef-layout--standard qodef-status--regular"
                                                             style="padding: 0px 30px">
                                                            <div class="qodef-m-inner">
                                                                <h4 class="qodef-m-title">
                                                                    <span>scotch</span>
                                                                </h4>
                                                                <div class="qodef-m-content">
                                                                    <p>Qui nemore
                                                                        molestie et
                                                                        civibus
                                                                        probatus</p>
                                                                </div>
                                                                <div class="qodef-m-image">
                                                                    <img width="346"
                                                                         height="332"
                                                                         src="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img3.png"
                                                                         class="attachment-full size-full"
                                                                         alt="jj"
                                                                         srcset="themes/singlemalt/wp-content/uploads/2020/09/h1-pricing-img3.png 346w, wp-content/uploads/2020/09/h1-pricing-img3-300x288.png 300w"
                                                                         sizes="(max-width: 346px) 100vw, 346px"/>
                                                                </div>
                                                                <p class="qodef-m-label">
                                                                    <span>Starting at</span>
                                                                </p>
                                                                <div class="qodef-m-price">
                                                                    <div class="qodef-m-price-wrapper qodef-h1"
                                                                         style="color: #232323">
                                                                                                            <span class="qodef-m-price-currency"
                                                                                                                  style="color: #232323">$</span>
                                                                        <span class="qodef-m-price-value">11300</span>
                                                                    </div>
                                                                </div>
                                                                <div class="qodef-m-button clear">
                                                                    <a class="qodef-shortcode qodef-m  qodef-button qodef-layout--outlined  qodef-html--link qodef-skin--default qodef-skin--default"
                                                                       href="product-category/fruit-liqueur/"
                                                                       target="_self"
                                                                       data-hover-color="#FFFFFF"
                                                                       data-hover-background-color="#BC9D72"
                                                                       style="color: #232323;font-size: 12px">
                                                                                                            <span class="qodef-m-text"> view more <span
                                                                                                                        class="qodef-button-arrow-svg"> <svg
                                                                                                                            version="1.1"
                                                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                                            x="0px"
                                                                                                                            y="0px"
                                                                                                                            width="14"
                                                                                                                            height="7"
                                                                                                                            viewBox="0 0 14.26 7.012"
                                                                                                                            enable-background="new 0 0 14.26 7.012"
                                                                                                                            xml:space="preserve"> <line
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                x1="0"
                                                                                                                                y1="3.506"
                                                                                                                                x2="13.553"
                                                                                                                                y2="3.506"/> <polyline
                                                                                                                                fill="none"
                                                                                                                                stroke-miterlimit="10"
                                                                                                                                points="10.4,0.354 13.553,3.507 10.4,6.659 "/> </svg> </span> </span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden elementor-section elementor-top-section elementor-element elementor-element-dd354bc elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
        data-id="dd354bc" data-element_type="section"
        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-8ee28ab"
                 data-id="8ee28ab" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-24f5dae elementor-widget__width-auto elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="24f5dae" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 11px 0px">
<span class="qodef-caption-line qodef--left"></span> The selection <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h2 class="qodef-m-title"
                                        style="color: #232323">
                                        our menu </h2>
                                    <p class="qodef-m-text" style="font-size: 15px">
                                        Movet mediocrem id vim, seaea etiam quando
                                        eloquentiam, his clita prompta pericula.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-f536a6b"
                 data-id="f536a6b" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-457eaaf elementor-hidden-phone elementor-widget elementor-widget-image"
                             data-id="457eaaf" data-element_type="widget"
                             data-widget_type="image.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-image">
                                    <img width="800" height="915"
                                         src="themes/singlemalt/wp-content/uploads/2020/09/h1-img-2.png"
                                         class="attachment-full size-full" alt="jj"
                                         srcset="themes/singlemalt/wp-content/uploads/2020/09/h1-img-2.png 800w, wp-content/uploads/2020/09/h1-img-2-262x300.png 262w, wp-content/uploads/2020/09/h1-img-2-768x878.png 768w, wp-content/uploads/2020/09/h1-img-2-600x686.png 600w"
                                         sizes="(max-width: 800px) 100vw, 800px"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-17a3fde"
                 data-id="17a3fde" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-b9baf2d elementor-widget elementor-widget-singlemalt_core_restaurant_menu_list"
                             data-id="b9baf2d" data-element_type="widget"
                             data-widget_type="singlemalt_core_restaurant_menu_list.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-restaurant-menu-list  qodef-grid qodef-layout--columns  qodef-gutter--medium qodef-col-num--1 qodef-item-layout--info-standard qodef-responsive--predefined">
                                    <div class="qodef-grid-inner clear">
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Single Pot Still </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $109</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Usu ad illum petentium error
                                                    feugait </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Tennessee </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $120</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Ne possit suavitate pri sint
                                                    erroribus </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Rye Whiskey </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $235</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Quot reprehendunt vim ne ceteros
                                                    dignissim </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Blended </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $199</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Vix dicta soleat concludaturque
                                                    an per </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Scotch </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $265</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Magna harum probatus ex eam
                                                    mea </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Bourbon </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $130</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Dicant habemus definitionem sed
                                                    ei elit </p>
                                            </div>
                                        </div>
                                        <div class="qodef-e qodef-grid-item">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-heading">
                                                    <h4 class="qodef-e-heading-title">
                                                        Grain Whiskey </h4>
                                                    <div class="qodef-e-heading-line"></div>
                                                    <p class="qodef-e-heading-price">
                                                        $215</p>
                                                </div>
                                                <p class="qodef-e-description">
                                                    Affert dolore pertinax at sit
                                                    meis omnis </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden qodef-row-frame qodef-background-text-section elementor-section elementor-top-section elementor-element elementor-element-7280060 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
        data-background-text="{&quot;text&quot;:&quot;unique&quot;,&quot;margin&quot;:&quot;45px 0px 0px 0px&quot;,&quot;margin_tablet&quot;:&quot;105px 0px 0px 0px&quot;,&quot;margin_phone&quot;:&quot;60px 0px 0px 0px&quot;,&quot;font_size&quot;:&quot;140px&quot;,&quot;color&quot;:&quot;#FAF7F2&quot;}"
        data-id="7280060" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-71ca314"
                 data-id="71ca314" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-5b5ed48 elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="5b5ed48" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 14px 0px ">
<span class="qodef-caption-line qodef--left"></span> They said <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h1 class="qodef-m-title"
                                        style="color: #232323">
                                        testimonials </h1>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-a727e74 elementor-widget elementor-widget-singlemalt_core_testimonials_list"
                             data-id="a727e74" data-element_type="widget"
                             data-widget_type="singlemalt_core_testimonials_list.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-testimonials-list  qodef-grid qodef-swiper-container  qodef-gutter--normal qodef-col-num--1 qodef-item-layout--info-below qodef-responsive--predefined"
                                     data-options="{&quot;slidesPerView&quot;:&quot;1&quot;,&quot;spaceBetween&quot;:15,&quot;loop&quot;:true,&quot;autoplay&quot;:true,&quot;speed&quot;:&quot;&quot;,&quot;speedAnimation&quot;:&quot;&quot;,&quot;unique&quot;:&quot;2&quot;,&quot;outsideNavigation&quot;:&quot;yes&quot;}">
                                    <div class="swiper-wrapper">
                                        <div class="qodef-e swiper-slide">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-content">
                                                    <p itemprop="description"
                                                       class="qodef-e-text">
                                                        Eum posidonium dissentias
                                                        id. Ne possit suavitate pri,
                                                        sint erroribus ut eam, eum
                                                        ea nullam repudiare. Affert
                                                        dolore pertinax at sit, meis
                                                        omnis causae vix ei ne
                                                        possit suavitate pri, sint
                                                        erroribus ut eam, eum ea
                                                        nullam. </p>
                                                    <h6 class="qodef-e-author">
                                                                                                <span class="qodef-e-author-name">
John Wilson	</span>
                                                        <span class="qodef-e-author-job">
</span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="qodef-e swiper-slide">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-content">
                                                    <p itemprop="description"
                                                       class="qodef-e-text">
                                                        Quot reprehendunt vim ne,
                                                        ceteros dignissim mnesarchum
                                                        ius ut, habemus persecuti
                                                        vis ad. Ne per scripta
                                                        intellegebat dicant habemus
                                                        definitionem sed ei, elit
                                                        gubergren sed ut, ne fugit
                                                        summo si suavitate. </p>
                                                    <h6 class="qodef-e-author">
                                                                                                <span class="qodef-e-author-name">
Michael Rogers	</span>
                                                        <span class="qodef-e-author-job">
</span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="qodef-e swiper-slide">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-content">
                                                    <p itemprop="description"
                                                       class="qodef-e-text">
                                                        Possit lucilius quaestio in
                                                        sea, sententiae reprimique
                                                        intellegam no usu. Ex munere
                                                        intellegebat eam ex quidam
                                                        labores interpretaris nec,
                                                        eum eu labore expetenda
                                                        suavitate vix dicta soleat
                                                        concludaturque an. </p>
                                                    <h6 class="qodef-e-author">
                                                                                                <span class="qodef-e-author-name">
Sonya Garner	</span>
                                                        <span class="qodef-e-author-job">
</span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>
                                <div class="swiper-button-next swiper-button-outside swiper-button-next-2">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px" y="0px" width="33.85px"
                                         height="19.958px"
                                         viewBox="0 0 33.85 19.958"
                                         enable-background="new 0 0 33.85 19.958"
                                         xml:space="preserve">
<line stroke-width="1.4" stroke-miterlimit="10" x1="0" y1="9.979" x2="33" y2="9.979"/>
                                        <polyline stroke-width="1.4"
                                                  stroke-miterlimit="10" points="23.376,0.495 32.859,9.979
            23.376,19.463 "/>
</svg>
                                </div>
                                <div class="swiper-button-prev swiper-button-outside swiper-button-prev-2">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px" y="0px" width="33.85px"
                                         height="19.958px"
                                         viewBox="0 0 33.85 19.958"
                                         enable-background="new 0 0 33.85 19.958"
                                         xml:space="preserve">
<line stroke-width="1.4" stroke-miterlimit="10" x1="0" y1="9.979" x2="33" y2="9.979"/>
                                        <polyline stroke-width="1.4"
                                                  stroke-miterlimit="10" points="23.376,0.495 32.859,9.979
            23.376,19.463 "/>
</svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden elementor-section elementor-top-section elementor-element elementor-element-d6590e7 qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
        data-id="d6590e7" data-element_type="section"
        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-2946366"
                 data-id="2946366" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-59ffc3b elementor-widget elementor-widget-singlemalt_core_counter"
                             data-id="59ffc3b" data-element_type="widget"
                             data-widget_type="singlemalt_core_counter.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                     data-start-digit="50" data-end-digit="107">
                                    <div class="qodef-m-digit-wrapper">
                                                                            <span class="qodef-m-digit"
                                                                                  style="color: #FFFFFF"></span></div>
                                    <div class="qodef-m-content">
                                        <p class="qodef-m-text"
                                           style="margin-top: 13px;color: #FFFFFF">
                                            Countries</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-66ee5cb"
                 data-id="66ee5cb" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-dc1a53b elementor-widget elementor-widget-singlemalt_core_counter"
                             data-id="dc1a53b" data-element_type="widget"
                             data-widget_type="singlemalt_core_counter.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                     data-start-digit="2050" data-end-digit="2136">
                                    <div class="qodef-m-digit-wrapper">
                                                                            <span class="qodef-m-digit"
                                                                                  style="color: #FFFFFF"></span></div>
                                    <div class="qodef-m-content">
                                        <p class="qodef-m-text"
                                           style="margin-top: 13px;color: #FFFFFF">
                                            Cases by day</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-4bfad17"
                 data-id="4bfad17" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-2f24cbd elementor-widget elementor-widget-singlemalt_core_counter"
                             data-id="2f24cbd" data-element_type="widget"
                             data-widget_type="singlemalt_core_counter.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                     data-start-digit="3200" data-end-digit="3285">
                                    <div class="qodef-m-digit-wrapper">
                                                                            <span class="qodef-m-digit"
                                                                                  style="color: #FFFFFF"></span></div>
                                    <div class="qodef-m-content">
                                        <p class="qodef-m-text"
                                           style="margin-top: 13px;color: #FFFFFF">
                                            Labels</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-8b40298"
                 data-id="8b40298" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-53a6477 elementor-widget elementor-widget-singlemalt_core_counter"
                             data-id="53a6477" data-element_type="widget"
                             data-widget_type="singlemalt_core_counter.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                     data-start-digit="90" data-end-digit="138">
                                    <div class="qodef-m-digit-wrapper">
                                                                            <span class="qodef-m-digit"
                                                                                  style="color: #FFFFFF"></span></div>
                                    <div class="qodef-m-content">
                                        <p class="qodef-m-text"
                                           style="margin-top: 13px;color: #FFFFFF">
                                            Collaborations</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-9a00274"
                 data-id="9a00274" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-1ba193d elementor-widget elementor-widget-singlemalt_core_counter"
                             data-id="1ba193d" data-element_type="widget"
                             data-widget_type="singlemalt_core_counter.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                     data-start-digit="4600" data-end-digit="4670">
                                    <div class="qodef-m-digit-wrapper">
                                                                            <span class="qodef-m-digit"
                                                                                  style="color: #FFFFFF"></span></div>
                                    <div class="qodef-m-content">
                                        <p class="qodef-m-text"
                                           style="margin-top: 13px;color: #FFFFFF">
                                            Labels</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hidden qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-66c4f2e qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
        data-id="66c4f2e"
        data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-a509954"
                 data-id="a509954" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-7803d90 elementor-widget elementor-widget-singlemalt_core_section_title"
                             data-id="7803d90" data-element_type="widget"
                             data-widget_type="singlemalt_core_section_title.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation">
                                                                            <span class="qodef-m-caption"
                                                                                  style="margin: 0px 0px 11px 0px">
<span class="qodef-caption-line qodef--left"></span> Contact us <span class="qodef-caption-line qodef--right"></span>
                                                                            </span>
                                    <h2 class="qodef-m-title"
                                        style="color: #232323;margin: -5px">
                                        get in touch </h2>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-8ca3420 elementor-widget elementor-widget-text-editor"
                             data-id="8ca3420" data-element_type="widget"
                             data-widget_type="text-editor.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-text-editor elementor-clearfix">
                                    <p style="text-align: center;"><span
                                                style="line-height: 29px;"><a
                                                    href="https://www.google.com/maps/place/66+Pine+Grove,+Lifford,+Ennis,+Co.+Clare,+V95+KX0D,+Ireland/@52.8557144,-8.9881115,17z/data=!3m1!4b1!4m5!3m4!1s0x485b6d356ede13d7:0xb42317390f19f0ec!8m2!3d52.8557112!4d-8.9859228"
                                                    target="_blank" rel="noopener">66 Pine Grove, Lifford, County Clare, Ireland </a><br/><a
                                                    href="https://www.google.com/maps/place/Ennis,+Co.+Clare,+Ireland/@52.8406417,-9.0121542,13z/data=!3m1!4b1!4m5!3m4!1s0x485b12cddf33d529:0xa00c7a997317320!8m2!3d52.8474255!4d-8.9887384"
                                                    target="_blank" rel="noopener">Ennis, County Clare</a>, </span>
                                        <span
                                                style="line-height: 29px;"><a
                                                    href="tel:2025550132">202-555-0132</a>, <a
                                                    href="tel:2025552154">202-555-2154</a><br/><a
                                                    href="/cdn-cgi/l/email-protection#f7849e99909b929a969b83b7869893929e998392859694839e8192d994989a"><span
                                                        class="__cf_email__"
                                                        data-cfemail="7a0913141d161f171b160e3a0b151e1f13140e1f081b190e130c1f54191517">[email&#160;protected]</span>,</a><br
                                            /><a href="/">www.singlemalt.com</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-94690a4 elementor-widget__width-auto elementor-widget elementor-widget-wp-widget-singlemalt_core_social_icons_group"
                             data-id="94690a4" data-element_type="widget"
                             data-widget_type="wp-widget-singlemalt_core_social_icons_group.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-social-icons-group">
                                                                            <span class="qodef-shortcode qodef-m  qodef-icon-holder  qodef-layout--circle "
                                                                                  style="width: 15px;height: 15px;line-height: 15px">
<a itemprop="url" href="https://twitter.com/QodeInteractive" target="_blank">
<span class="qodef-icon-font-awesome fab fa-twitter qodef-icon qodef-e" style="color: #232323"></span> <svg
            class="qodef-svg-circle">
<circle cx="50%" cy="50%" r="49%"></circle>
</svg>
                                                                            </a>
                                                                            </span><span
                                            class="qodef-shortcode qodef-m  qodef-icon-holder  qodef-layout--circle "
                                            style="width: 15px;height: 15px;line-height: 15px">
<a itemprop="url" href="https://www.facebook.com/QodeInteractive/" target="_blank">
<span class="qodef-icon-font-awesome fab fa-facebook-f qodef-icon qodef-e" style="color: #232323"></span> <svg
            class="qodef-svg-circle">
<circle cx="50%" cy="50%" r="49%"></circle>
</svg>
                                                                            </a>
                                                                            </span><span
                                            class="qodef-shortcode qodef-m  qodef-icon-holder  qodef-layout--circle "
                                            style="width: 15px;height: 15px;line-height: 15px">
<a itemprop="url" href="https://www.instagram.com/qodeinteractive/" target="_blank">
<span class="qodef-icon-font-awesome fab fa-instagram qodef-icon qodef-e" style="color: #232323"></span> <svg
            class="qodef-svg-circle">
<circle cx="50%" cy="50%" r="49%"></circle>
</svg>
                                                                            </a>
                                                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-4420548"
                 data-id="4420548" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-1b0e91d elementor-widget elementor-widget-image"
                             data-id="1b0e91d" data-element_type="widget"
                             data-widget_type="image.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-image">
                                    <img width="324" height="800"
                                         src="themes/singlemalt/wp-content/uploads/2020/07/h1-section-12-img-1.png"
                                         class="attachment-full size-full" alt="jj"
                                         srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-section-12-img-1.png 324w, wp-content/uploads/2020/07/h1-section-12-img-1-122x300.png 122w"
                                         sizes="(max-width: 324px) 100vw, 324px"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-afa902d"
                 data-id="afa902d" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-39a4a09 elementor-widget elementor-widget-wp-widget-singlemalt_core_contact_form_7"
                             data-id="39a4a09" data-element_type="widget"
                             data-widget_type="wp-widget-singlemalt_core_contact_form_7.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-contact-form-7">
                                    <div role="form" class="wpcf7"
                                         id="wpcf7-f5-p17-o2" lang="en-US"
                                         dir="ltr">
                                        <div class="screen-reader-response"
                                             role="alert" aria-live="polite"></div>
                                        <form action="/#wpcf7-f5-p17-o2"
                                              method="post" class="wpcf7-form init"
                                              novalidate="novalidate">
                                            <div style="display: none;">
                                                <input type="hidden" name="_wpcf7"
                                                       value="5"/>
                                                <input type="hidden"
                                                       name="_wpcf7_version"
                                                       value="5.2.2"/>
                                                <input type="hidden"
                                                       name="_wpcf7_locale"
                                                       value="en_US"/>
                                                <input type="hidden"
                                                       name="_wpcf7_unit_tag"
                                                       value="wpcf7-f5-p17-o2"/>
                                                <input type="hidden"
                                                       name="_wpcf7_container_post"
                                                       value="17"/>
                                                <input type="hidden"
                                                       name="_wpcf7_posted_data_hash"
                                                       value=""/>
                                            </div>
                                            <div class="qodef-contact-form-simple">
                                                <div class="qodef-contact-form-simple-input">
                                                    <label><span
                                                                class="wpcf7-form-control-wrap your-email"><input
                                                                    type="email"
                                                                    name="your-email"
                                                                    value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                    aria-required="true"
                                                                    aria-invalid="false"
                                                                    placeholder="E-mail"/></span>
                                                    </label>
                                                </div>
                                                <div class="qodef-contact-form-simple-input">
                                                    <label><span
                                                                class="wpcf7-form-control-wrap your-name"><input
                                                                    type="text"
                                                                    name="your-name"
                                                                    value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                    aria-required="true"
                                                                    aria-invalid="false"
                                                                    placeholder="Name"/></span>
                                                    </label>
                                                </div>
                                                <div class="qodef-contact-form-simple-text">
                                                    <label> <span
                                                                class="wpcf7-form-control-wrap your-message"><textarea
                                                                    name="your-message"
                                                                    cols="40" rows="6"
                                                                    class="wpcf7-form-control wpcf7-textarea"
                                                                    aria-invalid="false"
                                                                    placeholder="Message"></textarea></span>
                                                    </label>
                                                </div>
                                                <div class="qodef-contact-form-simple-submit">
                                                    <button type="submit"
                                                            class="wpcf7-form-control wpcf7-submit qodef-button qodef-size--normal qodef-type--filled qodef-m">
                                                        <span class="qodef-m-text"><?= System::translate('subscribe') ?></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="wpcf7-response-output"
                                                 role="alert"
                                                 aria-hidden="true"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background: #101010; display: flex; padding-top: 70px;">
    <div id="singlemalt_core_contact_form_7-2"
         class="widget widget_singlemalt_core_contact_form_7"
         data-area="footer_top_area_column_2" style="margin:auto;">
        <div class="qodef-contact-form-7">
            <div role="form" lang="en-US" dir="ltr">
                <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                <form action="newsletter" method="get" class="init">
                    <div class="qodef-newsletter-form">
                        <div class="qodef-newsletter-form-text">
                            <label>
                                <span class="wpcf7-form-control-wrap your-mail">
                                    <input type="email" name="your-mail" value="" size="40"
                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email"
                                           aria-invalid="false"
                                           placeholder="<?= System::translate('Your mail') ?>" required/>
                                </span>
                            </label>
                        </div>
                        <div class="qodef-newsletter-form-submit">
                            <button type="submit"
                                    class="wpcf7-form-control wpcf7-submit qodef-button qodef-size--normal qodef-type--filled qodef-m">
                                <span class="qodef-m-text"><?= System::translate('Submit') ?></span></button>
                        </div>
                    </div>
                    <div class="wpcf7-response-output" role="alert" aria-hidden="true"></div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(() => {
        const dashboard = new App.Dashboard();
        dashboard.openAgeVerification();
    })
</script>
