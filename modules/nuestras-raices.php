<div id="qodef-page-outer" style="padding-top: 0px;">
    <div class="qodef-page-title qodef-m qodef-title--standard qodef-alignment--center qodef-vertical-alignment--window-top qodef--has-image qodef-image--parallax qodef-parallax">
        <div class="qodef-m-inner">
            <div class="qodef-parallax-img-holder" style="opacity: 1;">
                <div class="qodef-parallax-img-wrapper" style="transform: translate3d(0px, 36.13%, 0px);">
                    <img width="1920" height="1200" src="assets/images/IMG%2012.jpg" class="qodef-parallax-img" alt="aa"
                         sizes="(max-width: 1920px) 100vw, 1920px">
                </div>
            </div>
            <div class="qodef-m-content qodef-content-grid qodef-parallax-content-holder">
                <h1 class="qodef-m-title entry-title"></h1>
            </div>
        </div>
    </div>
    <div id="qodef-page-inner" class="qodef-content-full-width">
        <main id="qodef-page-content" class="qodef-grid qodef-layout--template ">
            <div class="qodef-grid-inner clear">
                <div class="qodef-grid-item qodef-page-content-section qodef-col--12">
                    <div data-elementor-type="wp-page" data-elementor-id="2373" class="elementor elementor-2373"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <section
                                        class="qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-7bdcd03 elementor-section-full_width qodef-elementor-content-grid elementor-section-height-default elementor-section-height-default"
                                        data-id="7bdcd03" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dacc033"
                                                 data-id="dacc033" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-f58a74a elementor-widget elementor-widget-singlemalt_core_section_title"
                                                             data-id="f58a74a" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_section_title.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-section-title qodef-alignment--center  qodef-appear-animation qodef--appeared">
                                                                    <span class="qodef-m-caption"
                                                                          style="margin: 0px 0px 14px 0px">
                                                                        <span class="qodef-caption-line qodef--left"></span>
                                                                        <?= System::translate('nuestras-raices', 'modules') ?>
                                                                        <span class="qodef-caption-line qodef--right"></span>
                                                                    </span>
                                                                    <h1 class="qodef-m-title" style="color: #232323">
                                                                        <?= System::translate('Sobre Nosotros') ?>
                                                                    </h1>
                                                                    <p class="qodef-m-text"
                                                                       style="margin: 13px;color: #191919;font-size: 17px">

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-1fc36a0 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no"
                                                                data-id="1fc36a0" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                                <div class="elementor-row">
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0034791"
                                                                         data-id="0034791" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-cff1475 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="cff1475"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <h2 class="qodef-m-title"
                                                                                            style="color: #232323">
                                                                                            <?= System::translate('mision:title') ?>
                                                                                        </h2>
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p><?= System::translate('mision:content') ?></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-475d568 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="475d568"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-37aeb7f"
                                                                         data-id="37aeb7f" data-element_type="column">
                                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-8a9cb97 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="8a9cb97"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <h2 class="qodef-m-title"
                                                                                            style="color: #232323">
                                                                                            <?= System::translate('vision:title') ?>
                                                                                        </h2>
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p><?= System::translate('vision:content') ?></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-4068507 elementor-widget elementor-widget-text-editor"
                                                                                     data-id="4068507"
                                                                                     data-element_type="widget"
                                                                                     data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section
                                        class="hidden elementor-section elementor-top-section elementor-element elementor-element-2220357a qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                        data-id="2220357a" data-element_type="section"
                                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-40bbba50"
                                                 data-id="40bbba50" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-3ed318cb elementor-widget elementor-widget-singlemalt_core_counter"
                                                             data-id="3ed318cb" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_counter.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                                                     data-start-digit="50" data-end-digit="107">
                                                                    <div class="qodef-m-digit-wrapper">
                                                                        <span class="qodef-m-digit"
                                                                              style="color: #FFFFFF">107</span></div>
                                                                    <div class="qodef-m-content">
                                                                        <p class="qodef-m-text"
                                                                           style="margin-top: 13px;color: #FFFFFF">
                                                                            Countries</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-6e08f958"
                                                 data-id="6e08f958" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-6abefaf5 elementor-widget elementor-widget-singlemalt_core_counter"
                                                             data-id="6abefaf5" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_counter.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                                                     data-start-digit="2050" data-end-digit="2136">
                                                                    <div class="qodef-m-digit-wrapper">
                                                                        <span class="qodef-m-digit"
                                                                              style="color: #FFFFFF">2136</span></div>
                                                                    <div class="qodef-m-content">
                                                                        <p class="qodef-m-text"
                                                                           style="margin-top: 13px;color: #FFFFFF">Cases
                                                                            by day</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-381b2652"
                                                 data-id="381b2652" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-1f6bfaee elementor-widget elementor-widget-singlemalt_core_counter"
                                                             data-id="1f6bfaee" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_counter.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                                                     data-start-digit="3200" data-end-digit="3285">
                                                                    <div class="qodef-m-digit-wrapper">
                                                                        <span class="qodef-m-digit"
                                                                              style="color: #FFFFFF">3285</span></div>
                                                                    <div class="qodef-m-content">
                                                                        <p class="qodef-m-text"
                                                                           style="margin-top: 13px;color: #FFFFFF">
                                                                            Labels</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-1c790f33"
                                                 data-id="1c790f33" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-59b9ea28 elementor-widget elementor-widget-singlemalt_core_counter"
                                                             data-id="59b9ea28" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_counter.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                                                     data-start-digit="90" data-end-digit="138">
                                                                    <div class="qodef-m-digit-wrapper">
                                                                        <span class="qodef-m-digit"
                                                                              style="color: #FFFFFF">138</span></div>
                                                                    <div class="qodef-m-content">
                                                                        <p class="qodef-m-text"
                                                                           style="margin-top: 13px;color: #FFFFFF">
                                                                            Collaborations</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-1eb146ea"
                                                 data-id="1eb146ea" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-5ecd6f16 elementor-widget elementor-widget-singlemalt_core_counter"
                                                             data-id="5ecd6f16" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_counter.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-counter qodef-layout--simple"
                                                                     data-start-digit="4600" data-end-digit="4670">
                                                                    <div class="qodef-m-digit-wrapper">
                                                                        <span class="qodef-m-digit"
                                                                              style="color: #FFFFFF">4670</span></div>
                                                                    <div class="qodef-m-content">
                                                                        <p class="qodef-m-text"
                                                                           style="margin-top: 13px;color: #FFFFFF">
                                                                            Labels</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section
                                        class="hidden qodef-row-frame elementor-section elementor-top-section elementor-element elementor-element-379f3230 qodef-elementor-content-grid elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                        data-id="379f3230" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7fc05fe7"
                                                 data-id="7fc05fe7" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-6dfe4ca1 elementor-widget elementor-widget-singlemalt_core_item_showcase"
                                                             data-id="6dfe4ca1" data-element_type="widget"
                                                             data-widget_type="singlemalt_core_item_showcase.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="qodef-shortcode qodef-m  qodef-item-showcase qodef-layout--list qodef--init"
                                                                     style="background: url(themes/singlemalt/wp-content/uploads/2020/09/h1-item-showcase-background-img-1.png);background-repeat: no-repeat;background-position: center calc(50% + 60px)">
                                                                    <div class="qodef-m-items qodef--left">
                                                                        <div class="qodef-m-item qodef-e">
                                                                            <div class="qodef-e-item-image">
                                                                                <img width="98" height="142"
                                                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-1.png"
                                                                                     class="attachment-full size-full"
                                                                                     alt="jj"></div>
                                                                            <h3 class="qodef-e-title"
                                                                                style="color: #232323">
                                                                                malting </h3>
                                                                            <p class="qodef-e-text"
                                                                               style="color: #191919">Lorem ipsum dolor
                                                                                sit amet con tetur adi isicing elit sed
                                                                                do eius od tempor in incide</p>
                                                                        </div>
                                                                        <div class="qodef-m-item qodef-e">
                                                                            <div class="qodef-e-item-image">
                                                                                <img width="91" height="134"
                                                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-3.png"
                                                                                     class="attachment-full size-full"
                                                                                     alt="jj"></div>
                                                                            <h3 class="qodef-e-title"
                                                                                style="color: #232323">
                                                                                distillation </h3>
                                                                            <p class="qodef-e-text">Lorem ipsum dolor
                                                                                sit amet con tetur adi isicing elit sed
                                                                                do eius od tempor in incide</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="qodef-m-image" style="margin-top: 16px">
                                                                        <img width="238" height="824"
                                                                             src="themes/singlemalt/wp-content/uploads/2020/07/h1-showcase-item-img-1.png"
                                                                             class="attachment-full size-full" alt="jj"
                                                                             srcset="themes/singlemalt/wp-content/uploads/2020/07/h1-showcase-item-img-1.png 238w, themes/singlemalt/wp-content/uploads/2020/07/h1-showcase-item-img-1-87x300.png 87w"
                                                                             sizes="(max-width: 238px) 100vw, 238px">
                                                                    </div>
                                                                    <div class="qodef-m-items qodef--right">
                                                                        <div class="qodef-m-item qodef-e">
                                                                            <div class="qodef-e-item-image">
                                                                                <img width="96" height="168"
                                                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-2.png"
                                                                                     class="attachment-full size-full"
                                                                                     alt="jj"></div>
                                                                            <h3 class="qodef-e-title"
                                                                                style="color: #232323">
                                                                                mashing </h3>
                                                                            <p class="qodef-e-text"
                                                                               style="color: #191919">Lorem ipsum dolor
                                                                                sit amet con tetur adi isicing elit sed
                                                                                do eius od tempor in incide</p>
                                                                        </div>
                                                                        <div class="qodef-m-item qodef-e">
                                                                            <div class="qodef-e-item-image">
                                                                                <img width="82" height="137"
                                                                                     src="themes/singlemalt/wp-content/uploads/2020/07/h1-child-element-img-4.png"
                                                                                     class="attachment-full size-full"
                                                                                     alt="aa"></div>
                                                                            <h3 class="qodef-e-title"
                                                                                style="color: #232323">
                                                                                maturation </h3>
                                                                            <p class="qodef-e-text">Lorem ipsum dolor
                                                                                sit amet con tetur adi isicing elit sed
                                                                                do eius od tempor in incide</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
