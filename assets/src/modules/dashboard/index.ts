export class Dashboard {
    constructor() {
        this.checkAgeVerification();
    }

    checkAgeVerification() {
        if (document.cookie.indexOf('disabledAgeVerification=') == -1) {
            $('body').addClass('qodef-age-verification--opened');
        }
    }

    openAgeVerification() {
        qodefAgeVerificationModal.init();
    }
}

const qodefAgeVerificationModal = {
    init: function () {
        this.holder = $('#qodef-age-verification-modal');
        if (this.holder.length) {
            var $preventHolder = this.holder.find('.qodef-m-content-prevent');
            if ($preventHolder.length) {
                var $preventYesButton = $preventHolder.find('.qodef-prevent--yes');
                $preventYesButton.on('click', function () {
                    var cname = 'disabledAgeVerification';
                    var cvalue = 'Yes';
                    var exdays = 7;
                    var d = new Date();
                    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                    var expires = "expires=" + d.toUTCString();
                    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                    qodefAgeVerificationModal.handleClassAndScroll('remove');
                });
            }
        }
    },
    handleClassAndScroll: (option) => {
        const qodefCore = {
            body: undefined,
            html: undefined,
            qodefScroll: undefined
        };
        qodefCore.body = $('body');
        qodefCore.html = $('html');
        qodefCore.qodefScroll = qodefAgeVerificationModal.qodefScroll;
        if (option === 'remove') {
            qodefCore.body.removeClass('qodef-age-verification--opened');
            qodefCore.qodefScroll.enable();
        }
        if (option === 'add') {
            qodefCore.body.addClass('qodef-age-verification--opened');
            qodefCore.qodefScroll.disable();
        }
    },
    qodefScroll: {
        disable: function () {
            if (window.addEventListener) {
                window.addEventListener('wheel', qodefAgeVerificationModal.qodefScroll.preventDefaultValue, {
                    passive: false
                });
            }
            document.onkeydown = qodefAgeVerificationModal.qodefScroll.keyDown;
        },
        enable: function () {
            if (window.removeEventListener) {
                window.removeEventListener('wheel', qodefAgeVerificationModal.qodefScroll.preventDefaultValue, {
                    // @ts-ignore
                    passive: false
                });
            }
            // @ts-ignore
            window.onmousewheel = document.onmousewheel = document.onkeydown = null;
        },
        preventDefaultValue: function (e) {
            e = e || window.event;
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.returnValue = false;
        },
        keyDown: function (e) {
            var keys = [37, 38, 39, 40];
            for (var i = keys.length; i--;) {
                if (e.keyCode === keys[i]) {
                    qodefAgeVerificationModal.qodefScroll.preventDefaultValue(e);
                    return;
                }
            }
        }
    },
};
